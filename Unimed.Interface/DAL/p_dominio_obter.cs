﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unimed.DAL
{
    internal class p_dominio_obter
    {
        /// <summary>
        /// Obtem o nome do campo Chave do ECM
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="objRequest"></param>
        /// <returns></returns>
        public String Executar(SqlConnection conn, NameValueCollection objRequest)
        {
            String nm_retorno = String.Empty;

            try
            {
                Unimed.Shared.DATA.ExecutaProcedures exec = new Shared.DATA.ExecutaProcedures();
                exec.cdRetornoThrowErro = true;
                exec.ThrowErro = true;


                using (SqlDataReader dr = exec.ExecuteReader(conn, "[ged].[p_dominio_obter]", objRequest))
                {
                    if (!dr.HasRows)
                        throw new Exception("Dominio informado não encontrado");

                    if (dr.Read())
                    {
                        nm_retorno = dr["nm_chave"].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return nm_retorno;

        }
    }
}
