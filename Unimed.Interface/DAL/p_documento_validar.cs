﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unimed.Shared.Util;

namespace Unimed.DAL
{
    internal class p_documento_validar
    {
        public String Executar(SqlConnection conn, NameValueCollection objRequest)
        {
            String nm_retorno = String.Empty;

            try
            {
                Unimed.Shared.DATA.ExecutaProcedures exec = new Shared.DATA.ExecutaProcedures();
                exec.cdRetornoThrowErro = false;
                exec.ThrowErro = false;

                Object obj = exec.ExecuteNonQuery(conn, "[ged].[p_documento_validar]", objRequest, false);
                nm_retorno = exec.CdRetorno > 0 ? exec.NmRetorno : String.Empty;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return nm_retorno;

        }
    }
}
