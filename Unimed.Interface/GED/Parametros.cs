﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unimed.Shared.Util;
using Unimed.Shared.IO;

namespace Unimed.GED
{
    public class Parametros
    {
        public string RetornaChave(SqlConnection conn, NameValueCollection objRequest, Int32 id_dominio)
        {
            Helper.ObtemParametrosSistema(objRequest, "id_dominio", id_dominio);
            return new DAL.p_dominio_obter().Executar(conn, objRequest);
        }
        public static String ValidarArquivoUpload(SqlConnection conn, NameValueCollection objRequest, Byte[] file, String extensao)
        {
            Helper.ObtemParametrosSistema(objRequest, "nm_extensao", extensao);
            Helper.ObtemParametrosSistema(objRequest, "nr_tamanho_arquivo", Helper.ConvertBytesToMegabytes(file.GetLength(0)));

            return new DAL.p_documento_validar().Executar(conn, objRequest);
        }
        public static String DefinePastaArmazenamento(String nm_local_armazenamento, Int32 nr_quantidade_arquivos_pasta, String nm_pasta_atual, String nm_nome_arquivo)
        {
            Boolean dv_espaco_pasta = false;

            try
            {
                if (nm_pasta_atual.Equals(String.Empty))
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(nm_local_armazenamento);
                    DirectoryInfo[] ArrayDir = dirInfo.GetDirectories();

                    foreach (DirectoryInfo dir in ArrayDir)
                    {
                        /* VERIFICA SE É UMA PASTA CRIADA PELO SISTEMA (NUMÉRICA), SE NÃO FOR, IGNORA */
                        if (!Helper.IsNumeric(dir.Name))
                            continue;

                        nm_pasta_atual = dir.Name;

                        /* VERIFICA SE O LIMITE DE ARQUIVOS POR PASTA NAO FOI ALCANÇADO */
                        if (dir.GetFiles().GetLength(0) < nr_quantidade_arquivos_pasta)
                        {
                            dv_espaco_pasta = true;
                            break;
                        }
                    }
                }
                else
                {
                    dv_espaco_pasta = Directory.GetFiles(Path.Combine(nm_local_armazenamento, nm_pasta_atual)).GetLength(0) < nr_quantidade_arquivos_pasta;
                }

                if (!nm_pasta_atual.Equals(String.Empty) && (File.Exists(Path.Combine(nm_local_armazenamento, nm_pasta_atual, nm_nome_arquivo)) || !dv_espaco_pasta))
                {
                    if (Directory.Exists(Path.Combine(nm_local_armazenamento, (Convert.ToInt32(nm_pasta_atual) + 1).ToString("0000"))))
                    {
                        nm_pasta_atual = (Convert.ToInt32(nm_pasta_atual) + 1).ToString("0000");
                    }
                    else
                    {
                        nm_pasta_atual = FileDir.CriaPasta(nm_local_armazenamento, nm_pasta_atual, FileDir.tp_tratamento.GED);
                    }

                    return DefinePastaArmazenamento(nm_local_armazenamento, nr_quantidade_arquivos_pasta, nm_pasta_atual, nm_nome_arquivo);
                }

                //valida existencia de pastas criadas pelo engine
                if (nm_pasta_atual.Equals(String.Empty))
                {
                    FileDir.CriaPasta(Path.Combine(nm_local_armazenamento, "0001"));
                    return "0001";
                }
                else
                {
                    return nm_pasta_atual;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string ObtemValorRecursoParametro(SqlConnection conn, NameValueCollection objRequest, Int16 cd_parametro, Int16 id_recurso)
        {
            try
            {
                Helper.ObtemParametrosSistema(objRequest, "cd_parametro", cd_parametro);
                Helper.ObtemParametrosSistema(objRequest, "id_recurso", id_recurso);

                List<Unimed.Shared.EN.p_recurso_parametro> recurso = new Shared.DAL.p_recurso_parametro().Executar(conn, objRequest);
                return recurso[0].nm_valor.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
