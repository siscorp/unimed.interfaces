﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace Unimed.GED
{
    public class ImageService
    {
        public static Image DownResolution(Image image)
        {
            Bitmap original = (Bitmap)image;

            if (original.HorizontalResolution <= 70)
                throw new ApplicationException("A resolução da imagem já encontra-se no mínimo possível.");

            Size newSize = new Size(Convert.ToInt32((original.Width) * 0.70), Convert.ToInt32((original.Height) * 0.70));
            Bitmap bitmap = new Bitmap(original, newSize);
            bitmap.SetResolution(70, 70);

            return bitmap;
        }

        public static Bitmap ConvertToGrayscale(Image image)
        {
            Bitmap source = (Bitmap)image;
            Bitmap bm = new Bitmap(source.Width, source.Height);

            for (int y = 0; y < bm.Height; y++)
            {
                for (int x = 0; x < bm.Width; x++)
                {
                    Color c = source.GetPixel(x, y);
                    int luma = (int)(c.R * 0.3 + c.G * 0.59 + c.B * 0.11);
                    bm.SetPixel(x, y, Color.FromArgb(luma, luma, luma));
                }
            }
            return bm;
        }
    }
}
