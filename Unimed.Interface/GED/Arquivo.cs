﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unimed.Shared.Util;
using Unimed.Shared.IO;
using System.IO;
//using static System.Net.Mime.MediaTypeNames;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;

namespace Unimed.GED
{
    public class Arquivo
    {

        public Int32 InserirDocumento(SqlConnection conn, NameValueCollection objRequest, String id_tp_documento,
                                                                                            String id_dominio,
                                                                                            String nm_chave_valores,
                                                                                            String nm_arquivo,
                                                                                            String id_usuario,
                                                                                            String nm_observacao,
                                                                                            String nr_ref,
                                                                                            Byte[] Documento,
                                                                                            String TipoDocumento,
                                                                                            String dv_baixa_resolucao = "0")
        {

            String tp_armazenamento = String.Empty;
            String nm_local_armazenamento = String.Empty;
            String pastaArmazenamento = String.Empty;
            String pasta = String.Empty;
            Int32 nr_arquivos_pasta = 0;


            /* VALIDA O ARQUIVO PARA O UPLOAD */
            String validacao = GED.Parametros.ValidarArquivoUpload(conn, objRequest, Documento, Helper.Replace(Path.GetExtension(nm_arquivo), ".", String.Empty));

            if (!String.IsNullOrEmpty(validacao))
                throw new Exception(validacao);

            /* OBTEM O TIPO DE ARMAZENAMENTO DO GED */
            tp_armazenamento = Parametros.ObtemValorRecursoParametro(conn, objRequest, 2, 2);

            if (!dv_baixa_resolucao.Equals("0") && TipoDocumento.StartsWith("image/"))
            {
                using (MemoryStream msImageOriginal = new MemoryStream(Documento))
                {
                    using (Image image = ImageService.ConvertToGrayscale(ImageService.DownResolution(Image.FromStream(msImageOriginal))))
                    {
                        using (MemoryStream msImagemConvertida = new MemoryStream())
                        {
                            image.Save(msImagemConvertida, ImageFormat.Png);
                            Documento = msImagemConvertida.ToArray();
                            TipoDocumento = "image/png";
                        }
                    }
                }
            }

            if (tp_armazenamento.Equals("1"))
            {
                nm_local_armazenamento = Parametros.ObtemValorRecursoParametro(conn, objRequest, 5, 2);
                nr_arquivos_pasta = Convert.ToInt32(Parametros.ObtemValorRecursoParametro(conn, objRequest, 1, 2));

                pasta = Parametros.DefinePastaArmazenamento(nm_local_armazenamento, nr_arquivos_pasta, String.Empty, nm_arquivo);
                nm_arquivo = Path.Combine(pasta, nm_arquivo);

                if (!nm_local_armazenamento.EndsWith("\\"))
                {
                    nm_local_armazenamento += "\\";
                }
                nm_local_armazenamento = String.Concat(nm_local_armazenamento, nm_arquivo);

                using (BinaryWriter bw = new BinaryWriter(File.Open(nm_local_armazenamento, FileMode.Create)))
                {
                    bw.Write(Documento);
                }
            }

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "ecm.p_documento_inserir";
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.StoredProcedure;

                Unimed.Shared.DAL.p_deriva_parametros_procedure.DeriveParameters(cmd, false);

                Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@id_tp_documento"], id_tp_documento);
                Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@id_dominio"], id_dominio);
                Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@nm_chave_valores"], nm_chave_valores);
                Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@img_documento_arquivo"], nm_arquivo);

                if (!Convert.Equals(Documento, null) && tp_armazenamento.Trim().Equals("2"))
                {
                    Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@cd_tp_armazenamento"], "2");
                    Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@img_documento"], Documento);
                }
                else
                {
                    Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@cd_tp_armazenamento"], "1");
                    Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@img_documento"], DBNull.Value);
                }
                Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@img_documento_content_type"], TipoDocumento);
                Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@cd_usuario"], id_usuario);
                Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@dt_gravacao"], DateTime.Now);

                if (Unimed.Shared.Util.Helper.ConvertNull(nm_observacao).Equals(String.Empty))
                    Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@nm_observacao"], DBNull.Value);
                else
                    Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@nm_observacao"], nm_observacao);

                if (Unimed.Shared.Util.Helper.ConvertNull(nr_ref).Equals(String.Empty))
                    Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@nr_ref"], DBNull.Value);
                else
                    Unimed.Shared.Util.Parametros.DefinirValorParametro(cmd.Parameters["@nr_ref"], nr_ref);

                cmd.ExecuteNonQuery();

                Unimed.Shared.DATA.ExecutaProcedures exec = new Shared.DATA.ExecutaProcedures();
                exec.ExecuteNonQuery(cmd, objRequest);

                return Convert.ToInt32(cmd.Parameters["@id_documento"].Value);

            }

        }
        public static void Atualizar()
        {


        }

        public static void Excluir()
        {

        }

    }
}
