﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unimed.Joker.ServiceJoker;

namespace Unimed.Joker.DAL
{
    public class p_api_joker_dados_listar
    {
        public static void Listar(SqlConnection conn, NameValueCollection objRequest, createObjectType envelope)
        {
            Shared.DATA.ExecutaProcedures exec = new Shared.DATA.ExecutaProcedures();
            ArrayOfFieldTypeFieldField field = new ArrayOfFieldTypeFieldField();
            List<ArrayOfFieldTypeFieldField> fields = new List<ArrayOfFieldTypeFieldField>();

            try
            {
                conn.Open();

                using (SqlDataReader dr = exec.ExecuteReader(conn, "bpm.p_api_joker_configuracao_listar", objRequest))
                {
                    // abrir um loop pra 2 resultset
                    while(dr.Read())
                    {
                        envelope.objectModelUID = Convert.ToString(dr["objectModelUID"]); //"9be746a8-f88a-495b-962a-b3860f32964d";
                        //envelope.type = Convert.ToString(dr["type"]); //"SYNC";
                        envelope.contentName = Convert.ToString(dr["contentName"]); //"TesteCaio.pdf";
                        
                    }

                    dr.NextResult();

                    while (dr.Read())
                    {
                        field.name = Convert.ToString(dr["field_name"]);
                        field.value = Convert.ToString(dr["field_value"]);
                        
                        fields.Add(field);
                    }

                    envelope.fields = fields.ToArray();
                }

                conn.Close();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
