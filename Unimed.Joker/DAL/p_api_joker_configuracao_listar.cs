﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unimed.Joker.ServiceJoker;

namespace Unimed.Joker.DAL
{
    public class p_api_joker_configuracao_listar
    {
        public static void Listar(SqlConnection conn, NameValueCollection objRequest, createObjectType envelope, String nm_procedure)
        {
            Shared.DATA.ExecutaProcedures exec = new Shared.DATA.ExecutaProcedures();

            try
            {
                conn.Open();

                using (SqlDataReader dr = exec.ExecuteReader(conn, "bpm.p_api_joker_configuracao_listar", objRequest))
                {
                    if (dr.Read())
                    {
                        envelope.jokerIntegrationToken = Convert.ToString(dr["jokerIntegrationToken"]); //"JKR_TOK_#@_19";
                        envelope.userLogin = Convert.ToString(dr["userLogin"]); //"user_extrat_dev";
                        envelope.userPassword = Convert.ToString(dr["userPassword"]); //"Ext$20";
                        nm_procedure = Convert.ToString(dr["nm_procedure_formulario"]);

                        //envelope.objectModelUID = Convert.ToString(dr["objectModelUID"]); //"9be746a8-f88a-495b-962a-b3860f32964d";
                        ////envelope.type = Convert.ToString(dr["type"]); //"SYNC";
                        //envelope.contentName = Convert.ToString(dr["contentName"]); //"TesteCaio.pdf";
                        //envelope.fields = fields.ToArray();
                    }   
                }

                conn.Close();

            }catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
