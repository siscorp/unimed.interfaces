﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Data.SqlClient;

namespace Unimed.Joker
{
    using ServiceJoker;    

    public class Joker
    {
        public static void ProcessaDados(SqlConnection conn, NameValueCollection objRequest)
        {
            String nm_procedure_dados = String.Empty;
            createObjectType envelope = new createObjectType();

            try
            {
                /*BUSCA CONFIGURAÇÕES*/
                DAL.p_api_joker_configuracao_listar.Listar(conn, objRequest, envelope, nm_procedure_dados);

                /*BUSCA ESTRUTURA DO LAYOUT*/
                DAL.p_api_joker_dados_listar.Listar(conn, objRequest, envelope);

                /*REQUISIÇÃO*/
                using (var ws = new integrationJokerPortTypeSOAPBindingClient())
                {
                    var resposta = ws.createObject(envelope);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void Teste()
        {
            List<ArrayOfFieldTypeFieldField> fields = new List<ArrayOfFieldTypeFieldField>();

            createObjectType envelope = new createObjectType();

            envelope.jokerIntegrationToken = "JKR_TOK_#@_19";
            envelope.userLogin = "user_extrat_dev";
            envelope.userPassword = "Ext$20";
            envelope.objectModelUID = "9be746a8-f88a-495b-962a-b3860f32964d";
            //envelope.type = "SYNC";
            envelope.contentName = "TesteCaio.pdf";
            envelope.fields = fields.ToArray();

            
        }
    }
}
