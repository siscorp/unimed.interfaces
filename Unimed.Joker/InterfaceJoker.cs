﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using Unimed.Joker;

namespace Unimed
{
    public class InterfaceJoker
    {
        private System.Web.UI.Page _page;
        public System.Web.UI.Page page
        {
            get { return _page; }
            set { _page = value; }
        }

        private String _nome = String.Empty;
        public String nome
        {
            get { return _nome; }
            private set { _nome = value; }
        }

        private String _versao = String.Empty;
        public String versao
        {
            get { return _versao; }
            private set { _versao = value; }
        }

        private String _id_tela_redirect = String.Empty;
        public String id_tela_redirect
        {
            get { return _id_tela_redirect; }
            private set { _id_tela_redirect = value; }
        }

        private String g_CodRetorno = "0";
        public String CodRetorno
        {
            get { return g_CodRetorno; }
            private set { g_CodRetorno = value; }
        }

        private String g_StrRetorno = "Processamento realizado com sucesso.";
        public String StrRetorno
        {
            get
            {
                if (String.IsNullOrEmpty(g_StrRetorno) || String.IsNullOrWhiteSpace(g_StrRetorno))
                {
                    g_StrRetorno = "Processamento realizado com sucesso.";
                }
                return g_StrRetorno;
            }
            private set { g_StrRetorno = value; }
        }

        private SqlTransaction _trn = null;
        public SqlTransaction trn
        {
            get { return _trn; }
            set { _trn = value; }
        }

        public InterfaceJoker()
        {
            _nome = this.GetType().Assembly.ManifestModule.Name;
            _versao = this.GetType().Assembly.GetName().Version.ToString();
        }

        public void ExecutarVoid(SqlConnection conn, String nm_acao, NameValueCollection pobjRequest, out String nmRetorno, out String cdRetorno)
        {
            try
            {
                Object obj = Executar(conn, nm_acao, pobjRequest);
                nmRetorno = StrRetorno;
                cdRetorno = CodRetorno;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Método ExecutarWS 
        /// </summary>
        /// <param name="conn">Conexão de dados</param>
        /// <param name="nm_acao">Nome da ação para chamada do método</param>
        /// <param name="pobjRequest">Dicionario de dados</param>
        public String ExecutarWS(SqlConnection conn, String nm_acao, NameValueCollection pobjRequest)
        {
            object obj = Executar(conn, nm_acao, pobjRequest);

            return "";
        }

        /// <summary>
        /// Método Executar 
        /// </summary>
        /// <param name="conn">Conexão de dados</param>
        /// <param name="nm_acao">Nome da ação para chamada do método</param>
        /// <param name="pobjRequest">Dicionario de dados</param>
        public Object Executar(SqlConnection conn, String nm_acao, NameValueCollection pobjRequest)
        {

            System.Globalization.CultureInfo cultura = new System.Globalization.CultureInfo("pt-BR");
            System.Threading.Thread.CurrentThread.CurrentCulture = cultura;
            NameValueCollection objRequest = new NameValueCollection();

            int cd_retorno = 0;
            String nm_retorno = String.Empty;

            try
            {

                /* CONEXÃO DO COMPONENTE */
                //SqlConnection pConn = new ConnectionString().Conexao(conn, ConnectionString.Ambiente.ERP, ConnectionString.DevolveConexaoEntrada.SIM, pobjRequest);


                /* RECARREAGA A COLEÇÃO DE PARAMENTROS COM OS ITENS DA SESSION */
                Shared.Util.Helper.ObtemParametrosSistema(objRequest, pobjRequest);

                Shared.Util.Helper.ObtemParametrosSistema(pobjRequest, "nome", this.GetType().Assembly.GetName().Name);
                Shared.Util.Helper.ObtemParametrosSistema(pobjRequest, "versao", this.GetType().Assembly.GetName().Version.ToString());

                try
                {

                    /* PARAMETRO DE ENTRADA */
                    String acao = Shared.Util.Helper.ObtemParametrosSistema(objRequest, nm_acao);
                    switch (acao.ToLower())
                    {

                        case "gerarpdf":
                            Joker.Joker.ProcessaDados(conn, objRequest);
                            break;

                        default:
                            {
                                break;
                            }
                    }

                    g_CodRetorno = cd_retorno.ToString();
                    g_StrRetorno = nm_retorno;

                }
                catch (Exception ex)
                {

                    if ((cd_retorno <= 0))
                        g_CodRetorno = "1";
                    else
                        g_CodRetorno = cd_retorno.ToString();

                    g_StrRetorno = ex.Message;
                    throw ex;
                }

                Shared.Util.Helper.ObtemParametrosSistema(objRequest, "cd_retorno", g_CodRetorno);
                Shared.Util.Helper.ObtemParametrosSistema(objRequest, "nm_retorno", g_StrRetorno);

                /* FECHA A CONEXÃO DO COMPONENTE */
                //Shared.DAL.Data.CloseConnection(pConn);

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return objRequest;
        }
    }
}