﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

MERGE bpm.t_bpm_parametro AS TARGET
	USING (VALUES	(1,1,'Diretório padrão da aplicação','\\UNIMED02\Publico\Siscorp\GED'),
		   			(2,1,'Armazenamento: 1-Diretório, 2-Banco, 3-JokerDocs','1'),
					(3,1,'URL Serviço Joker',''),
					(4,1,'Tamanho máximo do arquivo em MB','25'),
					(1,2,'Caminho de Geração Padrão',''),
					(2,2,'Criptografia','1'),
					(3,2,'Implementar Seguranca?','1'),
					(1,3,'Remetente',''),
					(2,3,'E-Mail do Remetente',''),
					(3,3,'SMTP Usuáro',''),
					(4,3,'SMTP Senha',''),
					(5,3,'SMTP Servidor',''),
					(6,3,'Porta','587'),
					(7,3,'Utiliza SSL','')
					)
	AS SOURCE (cd_parametro,cd_recurso,nm_parametro,vl_parametro)
	ON (SOURCE.cd_parametro = TARGET.cd_parametro AND SOURCE.cd_recurso = TARGET.cd_recurso)
WHEN MATCHED THEN
	UPDATE 
	   SET	nm_parametro	= SOURCE.nm_parametro,
			vl_parametro	= SOURCE.vl_parametro
WHEN NOT MATCHED THEN
	INSERT(	cd_parametro,
			cd_recurso,
			nm_parametro,
			vl_parametro)
	VALUES(	SOURCE.cd_parametro,
			SOURCE.cd_recurso,
			SOURCE.nm_parametro,
			SOURCE.vl_parametro);