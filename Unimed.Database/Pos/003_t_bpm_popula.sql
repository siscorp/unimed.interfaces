﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
MERGE bpm.t_bpm AS TARGET
	USING (VALUES	(1,'GED', 'Gestão Eletrônica de Documento'),
					(2,'PDF', 'Portable Document Format'),
					(3,'EML', 'Eletronic Mail'),
					(4,'pWS', 'Provedor de Serviços Web'),
					(5,'cWS', 'Consumidor de Serviços Web'),
					(6,'APA', 'Agendador Processos Automaticos')
					)
	AS SOURCE (cd_recurso,nm_sigla,nm_recurso)
	ON (SOURCE.cd_recurso = TARGET.cd_recurso)
WHEN MATCHED  THEN
	UPDATE
	   SET nm_sigla = SOURCE.nm_sigla,
	       nm_recurso = SOURCE.nm_recurso
WHEN NOT MATCHED THEN
	INSERT(	cd_recurso,
			nm_sigla,
			nm_recurso)
	VALUES (SOURCE.cd_recurso,
			SOURCE.nm_sigla,
			SOURCE.nm_recurso);