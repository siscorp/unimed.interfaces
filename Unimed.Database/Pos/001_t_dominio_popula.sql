﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

SET IDENTITY_INSERT [ged].[t_dominio] ON 

	MERGE [ged].[t_dominio] AS TARGET
		USING (VALUES	(1,'Apólice','id_apolice', NULL),
						(2,'Endosso','id_endosso', NULL),
						(3,'Item','id_item', NULL),
						(4,'Pessoa','id_pessoa', NULL),
						(5,'Sinistro','nr_sinistro', NULL) )
		AS SOURCE (id_dominio,nm_dominio,nm_chave,nm_procedure_email)
		ON (SOURCE.id_dominio = TARGET.id_dominio)
	WHEN MATCHED THEN
		UPDATE
		   SET	nm_dominio				= SOURCE.nm_dominio,
				nm_chave				= SOURCE.nm_chave,
				nm_procedure_email		= SOURCE.nm_procedure_email
	WHEN NOT MATCHED THEN
		INSERT (id_dominio,
				nm_dominio,
				nm_chave,
				nm_procedure_email)
		VALUES (SOURCE.id_dominio,
				SOURCE.nm_dominio,
				SOURCE.nm_chave,
				SOURCE.nm_procedure_email);

SET IDENTITY_INSERT [ged].[t_dominio] OFF 
