﻿CREATE PROCEDURE [ged].[p_documento_validar]
(
	@nm_extensao								varchar(20),
	@nr_tamanho_arquivo							decimal(10, 2),

	@cd_usuario									VARCHAR (50)	= NULL	, 
	@cd_retorno 								INT				= NULL OUTPUT,
	@nm_retorno 								VARCHAR(255)	= NULL OUTPUT,
	@nr_versao_controle							VARCHAR(20)		= NULL OUTPUT
)	
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN
	
	SELECT	@nr_versao_controle = '$Revision: 1.1 $'

	BEGIN TRY

	DECLARE @nr_tamanho_default		INT

	/* VALIDA SE PERMITE REALIZAR UPLOAD DE ARQUIVOS DA EXTENSÃO PASSADA */
	IF EXISTS(SELECT TOP 1 1 FROM ged.t_extensoes  te WHERE te.nm_extensao = @nm_extensao AND te.dv_permite = 0)
		BEGIN
			SELECT	@cd_retorno = 1,
					@nm_retorno = 'Arquivos com a extensão "' + @nm_extensao + '" não são permitidos.'
			RETURN
		END

	/* OBTEM O TAMANHO PADRÃO DE ARQUIVOS DO PAINEL DE CONTROLE */
	SELECT @nr_tamanho_default = ISNULL(te.nr_tamanho_maximo, CONVERT(FLOAT,(	SELECT ISNULL(nm_valor, 0) 
																				FROM dbo.t_recurso_parametro trp 
																				WHERE trp.cd_parametro = 4 
																					AND trp.id_recurso = 2)))
	FROM ged.t_extensoes te
	WHERE te.nm_extensao = @nm_extensao

	IF (ISNULL(@nr_tamanho_default, 0) <> 0 and @nr_tamanho_arquivo > isnull(@nr_tamanho_default, 0))
		BEGIN
			
			SELECT	@cd_retorno = 1,
					@nm_retorno = 'O Arquivos com extensão "' + @nm_extensao + '" tem o tamanho máximo permitido de: "' + CONVERT(VARCHAR(50), @nr_tamanho_default) + 'em MB". O arquivo atual possuiu "' + CONVERT(VARCHAR(50), @nr_tamanho_arquivo) + '" em MB'
			RETURN
		END

					
		SELECT @cd_retorno = 0,
			   @nm_retorno =  'Processamento efetuado com sucesso.'

	END TRY
	BEGIN CATCH
		SELECT @cd_retorno = 1,
			   @nm_retorno =  'Erro na procedure de Validação do Arquivo: ' + ERROR_MESSAGE() 
								+ ' - Linha: ' + ERROR_LINE()
								+ ' - procedure: p_documento_validar'
	END CATCH
END