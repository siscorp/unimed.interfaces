﻿CREATE PROCEDURE [ged].[p_documento_incluir]
(
	@id_documento								INT				= NULL OUTPUT,
	@id_tp_documento							SMALLINT,
	@id_dominio									SMALLINT,
	@nm_chave_valores							BIGINT,
	@img_documento_arquivo						VARCHAR (300),
	@img_documento_content_type					VARCHAR (30)	= NULL,
	@img_documento								VARBINARY(MAX)	= NULL,
	@nr_ref										VARCHAR (30)	= NULL,
	@dt_gravacao								DATETIME		= NULL,
	@cd_tp_armazenamento						TINYINT			= NULL,
	@nm_observacao								VARCHAR (3000)	= NULL,
	@cd_usuario									VARCHAR (50)	= NULL , 
	@cd_retorno 								INT				= NULL OUTPUT,
	@nm_retorno 								VARCHAR(255)	= NULL OUTPUT,
	@nr_versao_controle							VARCHAR(20)		= NULL OUTPUT
)
AS

SET NOCOUNT ON

BEGIN
	
	SELECT	@nr_versao_controle = '$Revision: 1.1 $'

	BEGIN TRY
	
		 IF OBJECT_ID('tempdb..#output') IS NOT NULL 
			BEGIN
				DROP TABLE #output 
			END
		CREATE TABLE #output ( id_output INT )
			
		/* INSERE O REGISTRO */
		INSERT INTO ged.t_documento (id_tp_documento,id_dominio,nm_chave_valores,img_documento_arquivo,img_documento_content_type,nr_ref,cd_usuario,dt_gravacao ) 
		OUTPUT inserted.id_documento INTO #output (id_output)
		VALUES (@id_tp_documento,@id_dominio,@nm_chave_valores,@img_documento_arquivo,@img_documento_content_type,@nr_ref,@cd_usuario,GETDATE())

		SELECT TOP 1 @id_documento = id_output FROM #output

		/* INSERE O ARQUIVO */
		IF(ISNULL(@id_documento,0) > 0) AND (ISNULL(@cd_tp_armazenamento,0) = 2)
			BEGIN
				INSERT INTO ged.t_documento_arquivo(id_documento,img_documento)				
				VALUES (@id_documento,@img_documento)
			END

		SELECT @cd_retorno = 0,
			   @nm_retorno =  'Processamento efetuado com sucesso.'

	END TRY
	BEGIN CATCH
		SELECT @cd_retorno = 1,
			   @nm_retorno =  'Erro na procedure de Inclusão do Arquivo: ' + ERROR_MESSAGE() 
								+ ' - Linha: ' + ERROR_LINE()
								+ ' - procedure: p_documento_incluir'
	END CATCH
END