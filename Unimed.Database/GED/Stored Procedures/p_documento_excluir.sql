﻿CREATE PROCEDURE [ged].[p_documento_excluir]
(
	@id_documento								INT,

	@cd_retorno 								INT				= NULL OUTPUT,
	@nm_retorno 								VARCHAR(255)	= NULL OUTPUT,
	@nr_versao_controle							VARCHAR(20)		= NULL OUTPUT
)
AS

SET NOCOUNT ON

BEGIN
	
	SELECT	@nr_versao_controle = '$Revision: 1.1 $'

	BEGIN TRY
		
		/* EXCLUI O ARQUIVO */
		DELETE FROM ged.t_documento_arquivo 
		WHERE id_documento = @id_documento

		/* EXCLUI O REGISTRO DO ARQUIVO */
		DELETE FROM ged.t_documento
		WHERE id_documento = @id_documento


	END TRY
	BEGIN CATCH
		SELECT @cd_retorno = 1,
			   @nm_retorno =  'Erro na procedure de Exclusão do Arquivo: ' + ERROR_MESSAGE() 
								+ ' - Linha: ' + ERROR_LINE()
								+ ' - procedure: p_documento_excluir'
	END CATCH
END