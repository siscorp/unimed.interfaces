﻿CREATE PROCEDURE	.[p_documento_alterar]
(
	@id_documento								INT,
	@id_tp_documento							SMALLINT,
	@img_documento_arquivo						VARCHAR (300)	= NULL,
	@img_documento_content_type					VARCHAR (30)	= NULL,
	@img_documento								VARBINARY(MAX)	= NULL,
	@nr_ref										VARCHAR (30)	= NULL,
	@nm_observacao								VARCHAR (3000)	= NULL,
	@cd_tp_armazenamento						TINYINT			= NULL,
	@cd_usuario									VARCHAR (50)	= NULL ,
	@cd_retorno 								INT				= NULL OUTPUT,
	@nm_retorno 								VARCHAR(255)	= NULL OUTPUT,
	@nr_versao_controle							VARCHAR(20)		= NULL OUTPUT
)
AS

SET NOCOUNT ON

BEGIN
	
	SELECT	@nr_versao_controle = '$Revision: 1.1 $'

	BEGIN TRY

		/* ATUALIZA OS DADOS */
		UPDATE [ged].[t_documento]
		SET
			cd_usuario					= @cd_usuario,
			dt_gravacao					= GETDATE(),
			id_tp_documento				= @id_tp_documento,			
			img_documento_arquivo		= ISNULL(@img_documento_arquivo, img_documento_arquivo),
			img_documento_content_type	= ISNULL(@img_documento_content_type, img_documento_content_type),
			nm_observacao				= @nm_observacao,
			nr_ref						= @nr_ref			
		WHERE id_documento = @id_documento


		/* ATUALIZA O ARQUIVO */
		IF(ISNULL(@cd_tp_armazenamento,0) = 2)
			BEGIN
				UPDATE ged.t_documento_arquivo
				SET img_documento				= isnull(@img_documento, img_documento)
				WHERE id_documento = @id_documento
			END

		SELECT	@cd_retorno = 0,
				@nm_retorno =  'Processo efetuado com Sucesso.'

	END TRY
	BEGIN CATCH
		SELECT @cd_retorno = 1,
			   @nm_retorno =  'Erro na procedure de Alteração do Arquivo: ' + ERROR_MESSAGE() 
								+ ' - Linha: ' + ERROR_LINE()
								+ ' - procedure: p_documento_alterar'
	END CATCH
END