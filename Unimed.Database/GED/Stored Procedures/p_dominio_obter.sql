﻿CREATE PROCEDURE [ged].[p_dominio_obter]
(
	@id_dominio									SMALLINT,

	@cd_usuario									VARCHAR (50)	= NULL	, 
	@cd_retorno 								INT				= NULL OUTPUT,
	@nm_retorno 								VARCHAR(255)	= NULL OUTPUT,
	@nr_versao_controle							VARCHAR(20)		= NULL OUTPUT
)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN
	
	SELECT	@nr_versao_controle = '$Revision: 1.1 $'

	BEGIN TRY

		SELECT	td.id_dominio,
				td.nm_chave,
				td.nm_dominio,
				td.nm_procedure_email
		FROM ged.t_dominio td
		WHERE td.id_dominio = @id_dominio
		
		SELECT @cd_retorno = 0,
			   @nm_retorno =  'Processamento efetuado com sucesso.'

	END TRY
	BEGIN CATCH
		SELECT @cd_retorno = 1,
			   @nm_retorno =  'Erro na procedure para a Obtenção do Arquivo: ' + ERROR_MESSAGE() 
								+ ' - Linha: ' + ERROR_LINE()
								+ ' - procedure: p_documento_obter'	
	END CATCH
END
	
