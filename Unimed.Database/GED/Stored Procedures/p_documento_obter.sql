﻿CREATE PROCEDURE [ged].[p_documento_obter]
(
	@id_documento								INT				= NULL,
	@nm_chave_valores							BIGINT			= NULL,
	@id_dominio									INT				= NULL,

	@cd_usuario									VARCHAR (50)	= NULL	, 
	@cd_retorno 								INT				= NULL OUTPUT,
	@nm_retorno 								VARCHAR(255)	= NULL OUTPUT,
	@nr_versao_controle							VARCHAR(20)		= NULL OUTPUT
)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN
	
	SELECT	@nr_versao_controle = '$Revision: 1.1 $'

	BEGIN TRY
	
		SELECT
			td.[id_documento],
			td.[id_tp_documento],
			td.[id_dominio],
			td.[nm_chave_valores],
			td.[img_documento_arquivo],
			td.[img_documento_content_type],
			tda.[img_documento],
			td.[nr_ref],
			td.[dt_gravacao],
			td.[nm_observacao],
			td.[cd_usuario]			
		FROM ged.t_documento					td
			INNER JOIN ged.t_documento_arquivo	tda ON tda.id_documento = td.id_documento
		WHERE td.id_documento = @id_documento
			OR (td.id_dominio = @id_dominio 
				AND td.nm_chave_valores = @nm_chave_valores)
					
		SELECT @cd_retorno = 0,
			   @nm_retorno =  'Processamento efetuado com sucesso.'

	END TRY
	BEGIN CATCH
		SELECT @cd_retorno = 1,
			   @nm_retorno =  'Erro na procedure para a Obtenção do Arquivo: ' + ERROR_MESSAGE() 
								+ ' - Linha: ' + ERROR_LINE()
								+ ' - procedure: p_documento_obter'	
	END CATCH
END