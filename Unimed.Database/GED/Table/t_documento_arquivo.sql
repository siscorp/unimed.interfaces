﻿CREATE TABLE [ged].[t_documento_arquivo]
(
	[id_arquivo]	  INT IDENTITY (1, 1) NOT NULL,
	[id_documento]    INT NOT NULL,
	[img_documento]   VARBINARY(MAX) NULL,
	CONSTRAINT [pk_t_documento_arquivo(arquivo)] PRIMARY KEY CLUSTERED ([id_arquivo]),
	CONSTRAINT [fk_t_documento_arquivo_X_t_documento(id_documento)] FOREIGN KEY ([id_documento]) REFERENCES [ged].[t_documento] ([id_documento])
);
GO

CREATE NONCLUSTERED INDEX [idx_t_documento_arquivo(id_documento)]  ON [ged].[t_documento_arquivo]([id_documento]);
GO

