﻿CREATE TABLE [ged].[t_dominio_tp_documento]
(
	[id_dominio_tp_documento] INT     IDENTITY (1, 1) NOT NULL,
    [nm_dominio_tp_documento] VARCHAR (50) NOT NULL,
    CONSTRAINT [pk_t_dominio_tp_documento(id_dominio_tp_documento)] PRIMARY KEY CLUSTERED ([id_dominio_tp_documento] ASC)
);

