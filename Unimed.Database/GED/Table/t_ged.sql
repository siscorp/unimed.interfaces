﻿CREATE TABLE [ged].[t_ged](
	[cd_ged] [tinyint] NOT NULL,
	[cd_empresa] [int] NULL,
	[nm_descricao] [varchar](120) NULL,
 CONSTRAINT [pk_t_ged(cd_ged)] PRIMARY KEY CLUSTERED 
(
	[cd_ged] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
