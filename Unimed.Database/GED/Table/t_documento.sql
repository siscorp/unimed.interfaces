﻿CREATE TABLE [ged].[t_documento]
(
    [id_documento]               INT            IDENTITY (1, 1) NOT NULL,
    [id_tp_documento]            SMALLINT       NOT NULL,
    [id_dominio]                 SMALLINT       NOT NULL,
    [nm_chave_valores]           BIGINT         NOT NULL,
    [img_documento_arquivo]      VARCHAR (300)  NOT NULL,
    [img_documento_content_type] VARCHAR (100)   NULL,    
    [nr_ref]                     VARCHAR (30)   NULL,
    [dt_gravacao]                SMALLDATETIME  NOT NULL,
    [nm_observacao]              VARCHAR (3000) NULL,
    [cd_usuario]                 VARCHAR (50)   NOT NULL,    
    CONSTRAINT [pk_t_documento(id_documento)] PRIMARY KEY CLUSTERED ([id_documento]),
    CONSTRAINT [fk_t_documento_X_t_dominio_documento(id_tp_documento)] FOREIGN KEY ([id_tp_documento]) REFERENCES [ged].[t_dominio_documento] ([id_tp_documento]),
    CONSTRAINT [fk_t_documento_X_t_dominio(id_dominio)] FOREIGN KEY ([id_dominio]) REFERENCES [ged].[t_dominio] ([id_dominio])
);
GO

CREATE NONCLUSTERED INDEX [idx_t_documento(nm_chave_valores$id_dominio$dt_gravacao)] ON [ged].[t_documento]([nm_chave_valores] ASC, [id_dominio] ASC, [dt_gravacao] DESC);
GO

CREATE NONCLUSTERED INDEX [idx_t_documento(img_documento_arquivo)]  ON [ged].[t_documento]([img_documento_arquivo]);
GO