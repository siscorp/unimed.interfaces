﻿CREATE TABLE [ged].[t_extensoes]
(
	[id_extensao] INT IDENTITY (1,1) NOT NULL , 
    [nm_extensao] VARCHAR(20) NOT NULL, 
    [nr_tamanho_maximo] INT NULL, 
    [dv_permite] BIT NULL ,
    CONSTRAINT [pk_t_extensoes(id_extensao)] PRIMARY KEY CLUSTERED ([id_extensao] ASC)
); 

GO

CREATE UNIQUE INDEX [idx_t_extensoes(nm_extensao)] ON [ged].[t_extensoes] ([nm_extensao])