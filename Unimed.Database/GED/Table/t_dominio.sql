﻿CREATE TABLE [ged].[t_dominio] (
    [id_dominio]         SMALLINT       IDENTITY (1, 1) NOT NULL,
    [nm_dominio]         VARCHAR (30)   NOT NULL,
    [nm_chave]           VARCHAR (300)  NOT NULL,
    [nm_procedure_email] VARCHAR (5000) NULL,
    CONSTRAINT [pk_t_dominio(id_dominio)] PRIMARY KEY CLUSTERED ([id_dominio] ASC)
);

