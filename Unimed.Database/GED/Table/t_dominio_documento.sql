﻿CREATE TABLE [ged].[t_dominio_documento]
(
	[id_tp_documento]     SMALLINT     IDENTITY (1, 1) NOT NULL,
    [nm_tp_documento]     VARCHAR (30) NOT NULL,
    [id_grp_documento]    TINYINT      NOT NULL,
    [id_dominio_tp_documento] INT          NULL,
    CONSTRAINT [pk_t_dominio_documento(id_tp_documento)] PRIMARY KEY CLUSTERED ([id_tp_documento] ASC),
    FOREIGN KEY ([id_dominio_tp_documento]) REFERENCES [ged].[t_dominio_tp_documento] ([id_dominio_tp_documento]),
    CONSTRAINT [fk_t_ecm_dom_documento_t_grp_documento] FOREIGN KEY ([id_grp_documento]) REFERENCES [ged].[t_grp_documento] ([id_grp_documento])
)
