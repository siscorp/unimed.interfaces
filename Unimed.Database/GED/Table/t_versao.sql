﻿CREATE TABLE [ged].[t_versao] 
(   [id_versao]      INT IDENTITY(1,1) NOT NULL,
    [nr_versao]     VARCHAR (20) NOT NULL,
    [dt_aplicacao]  DATETIME
);
GO

CREATE UNIQUE INDEX [idx_t_versao(id_versao)] ON [ged].[t_versao] ([id_versao]) 