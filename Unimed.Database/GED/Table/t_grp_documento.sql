﻿CREATE TABLE [ged].[t_grp_documento]
(
	[id_grp_documento] TINYINT      IDENTITY (1, 1) NOT NULL,
    [nm_grp_documento] VARCHAR (30) NOT NULL,
    [id_dominio]       SMALLINT     NULL,
    CONSTRAINT [pk_t_grp_documento] PRIMARY KEY CLUSTERED ([id_grp_documento] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [fk_t_grp_documento_X_t_dominio(id_dominio)] FOREIGN KEY ([id_dominio]) REFERENCES [ged].[t_dominio] ([id_dominio])
)
