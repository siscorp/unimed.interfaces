﻿CREATE TABLE [ged].[t_grp_documento_grupo]
(
	[id_grp_documento_grupo] INT     IDENTITY (1, 1) NOT NULL,
    [id_grp_documento]       TINYINT NOT NULL,
    [id_grupo]               TINYINT NOT NULL,
    [dv_acesso]              BIT     NOT NULL,
    CONSTRAINT [pk_t_grp_documento_grupo(id_grp_documento_grupo)] PRIMARY KEY CLUSTERED ([id_grp_documento_grupo] ASC),
    CONSTRAINT [fk_t_grp_documento_grupo_X_t_grp_documento(id_grp_documento)] FOREIGN KEY ([id_grp_documento]) REFERENCES [ged].[t_grp_documento] ([id_grp_documento])
)
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_t_grp_documento_grupo(id_grp_documento)] ON [ged].[t_grp_documento_grupo]([id_grp_documento] ASC, [id_grupo] ASC);