﻿CREATE TABLE [ged].[t_documento_parametro]
(
	cd_parametro			INT NOT NULL,
	nm_parametro			VARCHAR(80) NULL,
	vl_prarametro			INT NULL,
	dv_parametro			BIT NULL
	CONSTRAINT [pk_t_documento_parametro(cd_parametro)] PRIMARY KEY CLUSTERED ([cd_parametro]),
);
