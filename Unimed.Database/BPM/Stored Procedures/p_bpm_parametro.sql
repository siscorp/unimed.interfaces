﻿CREATE PROCEDURE [bpm].[p_bpm_parametro]
(
	@id_parametro								SMALLINT		= NULL,
	@cd_parametro								SMALLINT		= NULL,
	@cd_recurso									TINYINT			= NULL,

	@cd_retorno 								INT				= NULL OUTPUT,
	@nm_retorno 								VARCHAR(255)	= NULL OUTPUT,
	@nr_versao_controle							VARCHAR(20)		= NULL OUTPUT
)
AS

SET NOCOUNT ON

BEGIN
	
	SELECT	@nr_versao_controle = '$Revision: 1.1 $'

	BEGIN TRY
		
		IF(ISNULL(@id_parametro,0) = 0 AND (ISNULL(@cd_parametro,0) = 0 AND ISNULL(@cd_recurso,0) = 0))
			BEGIN
				SELECT	@cd_retorno = 1,
						@nm_retorno = 'Parametro de Busca do Recurso não informado.'
				RETURN
			END

		SELECT	id_parametro	= tbpp.id_parametro,
				cd_parametro	= tbpp.cd_parametro,
				cd_recurso		= tbpp.cd_recurso,
				nm_parametro	= tbpp.nm_parametro,
				nm_valor		= ISNULL(tbp.vl_parametro,'')
		FROM bpm.t_bpm_parametro tbp
		WHERE tbp.id_parametro = ISNULL(@id_parametro,tbp.id_parametro)
			OR (tbp.cd_parametro = @cd_parametro and tbp.cd_recurso = @cd_recurso)

		SELECT @cd_retorno = 0,
			   @nm_retorno =  'Processo efetuado com sucesso.'
	END TRY
	BEGIN CATCH
		SELECT @cd_retorno = 1,
			   @nm_retorno =  'Erro na procedure de Busca de Parametros de Recurso: ' + ERROR_MESSAGE() 
								+ ' - Linha: ' + ERROR_LINE()
								+ ' - procedure: p_recurso_parametro'
	END CATCH
END