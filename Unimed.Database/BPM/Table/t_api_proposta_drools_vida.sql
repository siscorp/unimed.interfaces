﻿CREATE TABLE [bpm].[t_api_proposta_drools_vida](
	[id_drools_vida] [int] IDENTITY(1,1) NOT NULL,
	[id_proposta_drools] [int] NULL,
	[vl_altura] [numeric](19, 2) NULL,
	[cd_prodissao] [int] NULL,
	[vl_da] [numeric](19, 2) NULL,
	[dt_nascimento] [smalldatetime] NULL,
	[cd_estado_civil] [int] NULL,
	[dv_fumante] [bit] NULL,
	[nr_fuma_tempo] [int] NULL,
	[id_vida] [int] NULL,
	[vl_margem] [numeric](19, 2) NULL,
	[cd_natureza_operacao] [int] NULL,
	[pe_autonomo] [numeric](19, 2) NULL,
	[vl_peso] [numeric](19, 2) NULL,
	[cd_relacionamento_unimed] [int] NULL,
	[vl_renda_mensal] [numeric](19, 2) NULL,
	[cd_sexo] [int] NULL,
	[cd_tp_vida] [int] NULL,
 CONSTRAINT [PK_t_api_proposta_drools_vida(id_drools_vida)] PRIMARY KEY CLUSTERED 
(
	[id_drools_vida] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_drools_vida]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_drools_vida_X_t_api_proposta_drools(id_proposta_drools)] FOREIGN KEY([id_proposta_drools])
REFERENCES [bpm].[t_api_proposta_drools] ([id_proposta_drools])
GO

ALTER TABLE [bpm].[t_api_proposta_drools_vida] CHECK CONSTRAINT [FK_t_api_proposta_drools_vida_X_t_api_proposta_drools(id_proposta_drools)]
GO