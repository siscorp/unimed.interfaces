﻿CREATE TABLE [bpm].[t_api_proposta_drools_corretor](
	[id_drools_corretor] [int] IDENTITY(1,1) NOT NULL,
	[id_proposta_drools] [int] NULL,
	[cd_corretor] [int] NULL,
	[dv_principal] [bit] NULL,
	[vl_taxa_comissao] [numeric](19, 2) NULL,
	[cd_tp_comissao] [int] NULL,
 CONSTRAINT [PK_t_api_proposta_drools_corretor(id_drools_corretor)] PRIMARY KEY CLUSTERED 
(
	[id_drools_corretor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_drools_corretor]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_drools_corretor_X_t_api_proposta_drools(id_proposta_drools)] FOREIGN KEY([id_proposta_drools])
REFERENCES [bpm].[t_api_proposta_drools] ([id_proposta_drools])
GO

ALTER TABLE [bpm].[t_api_proposta_drools_corretor] CHECK CONSTRAINT [FK_t_api_proposta_drools_corretor_X_t_api_proposta_drools(id_proposta_drools)]
GO