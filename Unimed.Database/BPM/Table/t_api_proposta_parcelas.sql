﻿CREATE TABLE [bpm].[t_api_proposta_parcelas](
	[id_parcela] [int] IDENTITY(1,1) NOT NULL,
	[id_proposta] [int] NULL,
	[nr_parcela] [int] NULL,
	[vl_premio] [numeric](19, 2) NULL,
	[vl_iof_premio] [numeric](19, 2) NULL,
	[vl_taxa_cobertura] [numeric](19, 2) NULL,
	[vl_total_assistencias] [numeric](19, 2) NULL,
	[vl_juros] [numeric](19, 2) NULL,
	[vl_iof_juros] [numeric](19, 2) NULL,
 CONSTRAINT [PK_t_api_proposta_parcelas(id_parcela)] PRIMARY KEY CLUSTERED 
(
	[id_parcela] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_parcelas]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_parcelas_X_t_api_proposta(id_proposta)] FOREIGN KEY([id_proposta])
REFERENCES [bpm].[t_api_proposta] ([id_proposta])
GO

ALTER TABLE [bpm].[t_api_proposta_parcelas] CHECK CONSTRAINT [FK_t_api_proposta_parcelas_X_t_api_proposta(id_proposta)]
GO