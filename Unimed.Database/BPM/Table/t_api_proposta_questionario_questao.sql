﻿CREATE TABLE [bpm].[t_api_proposta_questionario_questao](
	[id_questao] [int] IDENTITY(1,1) NOT NULL,
	[id_questionario] [int] NULL,
	[cd_pergunta] [int] NULL,
	[nm_resposta] [varchar](200) NULL,
	[ds_resposta] [varchar](200) NULL,
 CONSTRAINT [PK_t_api_proposta_questionario_questao(id_questao)] PRIMARY KEY CLUSTERED 
(
	[id_questao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_questionario_questao]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_questionario_questao_X_t_api_proposta_questionario(id_questionario)] FOREIGN KEY([id_questionario])
REFERENCES [bpm].[t_api_proposta_questionario] ([id_questionario])
GO

ALTER TABLE [bpm].[t_api_proposta_questionario_questao] CHECK CONSTRAINT [FK_t_api_proposta_questionario_questao_X_t_api_proposta_questionario(id_questionario)]
GO