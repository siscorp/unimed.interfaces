﻿CREATE TABLE [bpm].[t_api_proposta_drools](
	[id_proposta_drools] [int] IDENTITY(1,1) NOT NULL,
	[id_proposta] [int] NULL,
	[nm_retorno_ocorrencia] [varchar](max) NULL,
	[cd_tipo_regra] [int] NULL,
	[cd_regra] [int] NULL,
	[ds_regra] [varchar](max) NULL,
	[cd_cotacao] [int] NULL,
	[cd_parceiro] [int] NULL,
	[cd_produto] [varchar](80) NULL,
	[dt_cotacao] [smalldatetime] NULL,
	[dt_fim_apo] [smalldatetime] NULL,
	[dt_fim_prop] [smalldatetime] NULL,
	[cd_forma_pagamento] [int] NULL,
	[dt_inicio_apo] [smalldatetime] NULL,
	[dt_inicio_prop] [smalldatetime] NULL,
	[cd_periodicidade_pag] [int] NULL,
	[nr_proposta] [int] NULL,
	[cd_sistema_origem] [int] NULL,
	[cd_tp_vigencia] [int] NULL,
 CONSTRAINT [PK_t_api_proposta_drools(id_proposta_drools)] PRIMARY KEY CLUSTERED 
(
	[id_proposta_drools] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_drools]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_drools_X_t_api_proposta(id_proposta)] FOREIGN KEY([id_proposta])
REFERENCES [bpm].[t_api_proposta] ([id_proposta])
GO

ALTER TABLE [bpm].[t_api_proposta_drools] CHECK CONSTRAINT [FK_t_api_proposta_drools_X_t_api_proposta(id_proposta)]
GO