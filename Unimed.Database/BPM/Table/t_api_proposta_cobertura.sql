﻿CREATE TABLE [bpm].[t_api_proposta_cobertura](
	[id_cobertura] [int] IDENTITY(1,1) NOT NULL,
	[id_proposta] [int] NULL,
	[nm_cd_cobertura] [varchar](20) NULL,
	[nm_capital] [varchar](80) NULL,
	[nm_premio_liquido] [varchar](80) NULL,
	[nm_taxa] [varchar](80) NULL,
	[nm_premio_comercial] [varchar](80) NULL,
	[nm_cod_produto] [varchar](100) NULL,
	[nm_cod_ramo] [varchar](20) NULL,
	[vl_carregamento] [numeric](19, 2) NULL,
	[vl_carregamento_risco] [numeric](19, 2) NULL,
	[vl_margem] [numeric](19, 2) NULL,
	[vl_corretagem] [numeric](19, 2) NULL,
	[vl_prolabore] [numeric](19, 2) NULL,
	[vl_agenciamento] [numeric](19, 2) NULL,
	[nm_vl_iof] [varchar](20) NULL,
	[vl_da] [numeric](19, 2) NULL,
 CONSTRAINT [PK_t_api_proposta_cobertura(id_cobertura)] PRIMARY KEY CLUSTERED 
(
	[id_cobertura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_cobertura]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_cobertura_X_t_api_proposta(id_proposta)] FOREIGN KEY([id_proposta])
REFERENCES [bpm].[t_api_proposta] ([id_proposta])
GO

ALTER TABLE [bpm].[t_api_proposta_cobertura] CHECK CONSTRAINT [FK_t_api_proposta_cobertura_X_t_api_proposta(id_proposta)]
GO