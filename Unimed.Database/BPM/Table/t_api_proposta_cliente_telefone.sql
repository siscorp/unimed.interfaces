﻿CREATE TABLE [bpm].[t_api_proposta_cliente_telefone](
	[id_telefone] [int] IDENTITY(1,1) NOT NULL,
	[id_proposta] [int] NULL,
	[nm_tipo] [varchar](30) NULL,
	[nr_ddd] [int] NULL,
	[nr_telefone] [bigint] NULL,
 CONSTRAINT [PK_t_api_proposta_cliente_telefone(id_telefone)] PRIMARY KEY CLUSTERED 
(
	[id_telefone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_cliente_telefone]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_cliente_telefone_X_t_api_proposta(id_proposta)] FOREIGN KEY([id_proposta])
REFERENCES [bpm].[t_api_proposta] ([id_proposta])
GO

ALTER TABLE [bpm].[t_api_proposta_cliente_telefone] CHECK CONSTRAINT [FK_t_api_proposta_cliente_telefone_X_t_api_proposta(id_proposta)]
GO