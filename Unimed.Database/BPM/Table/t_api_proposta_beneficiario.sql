﻿CREATE TABLE [bpm].[t_api_proposta_beneficiario](
	[id_beneficiario] [int] IDENTITY(1,1) NOT NULL,
	[id_proposta] [int] NULL,
	[nm_beneficiario] [varchar](150) NULL,
	[nm_cpf] [varchar](20) NULL,
	[cd_grau_parentesco] [int] NULL,
	[dt_nascimento] [smalldatetime] NULL,
	[pe_participacao] [numeric](19, 2) NULL,
 CONSTRAINT [PK_t_api_proposta_beneficiario(id_beneficiario)] PRIMARY KEY CLUSTERED 
(
	[id_beneficiario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_beneficiario]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_beneficiario_X_t_api_proposta(id_proposta)] FOREIGN KEY([id_proposta])
REFERENCES [bpm].[t_api_proposta] ([id_proposta])
GO

ALTER TABLE [bpm].[t_api_proposta_beneficiario] CHECK CONSTRAINT [FK_t_api_proposta_beneficiario_X_t_api_proposta(id_proposta)]
GO