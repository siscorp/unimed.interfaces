﻿CREATE TABLE [bpm].[t_api_proposta_cliente_endereco](
	[id_endereco] [int] IDENTITY(1,1) NOT NULL,
	[id_proposta] [int] NULL,
	[nm_bairro] [varchar](50) NULL,
	[nm_cep] [varchar](50) NULL,
	[nm_cidade] [varchar](50) NULL,
	[nm_ibge_municipio] [varchar](50) NULL,
	[nm_complemento] [varchar](50) NULL,
	[nm_logradouro] [varchar](50) NULL,
	[cd_estado] [int] NULL,
	[nr_endereco] [int] NULL,
	[nm_tipo_logradouro] [varchar](5) NULL,
	[nm_tipo_mais_endereco] [varchar](5) NULL,
 CONSTRAINT [PK_t_api_proposta_cliente_endereco(id_endereco)] PRIMARY KEY CLUSTERED 
(
	[id_endereco] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_cliente_endereco]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_cliente_endereco_X_t_api_proposta(id_proposta)] FOREIGN KEY([id_proposta])
REFERENCES [bpm].[t_api_proposta] ([id_proposta])
GO

ALTER TABLE [bpm].[t_api_proposta_cliente_endereco] CHECK CONSTRAINT [FK_t_api_proposta_cliente_endereco_X_t_api_proposta(id_proposta)]
GO