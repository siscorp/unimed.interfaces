﻿
CREATE TABLE [bpm].[t_bpm](
	[cd_recurso] [tinyint] NOT NULL,
	[nm_sigla] [varchar](3) NOT NULL,
	[nm_recurso] [varchar](120) NOT NULL,
 CONSTRAINT [pk_t_bpm(cd_recurso)] PRIMARY KEY CLUSTERED 
(
	[cd_recurso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

