﻿CREATE TABLE [bpm].[t_api_proposta_questionario](
	[id_questionario] [int] IDENTITY(1,1) NOT NULL,
	[id_proposta] [int] NULL,
	[vl_peso] [numeric](19, 2) NULL,
	[vl_altura] [numeric](19, 2) NULL,
	[dv_fumante] [bit] NULL,
	[nr_fuma_tempo] [int] NULL,
	[dt_documento] [smalldatetime] NULL,
 CONSTRAINT [PK_t_api_proposta_questionario(id_questionario)] PRIMARY KEY CLUSTERED 
(
	[id_questionario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_questionario]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_questionario_X_t_api_proposta(id_proposta)] FOREIGN KEY([id_proposta])
REFERENCES [bpm].[t_api_proposta] ([id_proposta])
GO

ALTER TABLE [bpm].[t_api_proposta_questionario] CHECK CONSTRAINT [FK_t_api_proposta_questionario_X_t_api_proposta(id_proposta)]
GO