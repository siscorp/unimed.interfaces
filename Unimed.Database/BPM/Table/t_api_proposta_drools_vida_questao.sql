﻿CREATE TABLE [bpm].[t_api_proposta_drools_vida_questao](
	[id_vida_questao] [int] IDENTITY(1,1) NOT NULL,
	[id_drools_vida] [int] NULL,
	[ds_resposta] [varchar](200) NULL,
	[cd_pergunta] [int] NULL,
	[nm_resposta] [varchar](200) NULL,
 CONSTRAINT [PK_t_api_proposta_drools_vida_questao(id_vida_questao)] PRIMARY KEY CLUSTERED 
(
	[id_vida_questao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_drools_vida_questao]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_drools_vida_questao_X_t_api_proposta_drools_vida(id_drools_vida)] FOREIGN KEY([id_drools_vida])
REFERENCES [bpm].[t_api_proposta_drools_vida] ([id_drools_vida])
GO

ALTER TABLE [bpm].[t_api_proposta_drools_vida_questao] CHECK CONSTRAINT [FK_t_api_proposta_drools_vida_questao_X_t_api_proposta_drools_vida(id_drools_vida)]
GO