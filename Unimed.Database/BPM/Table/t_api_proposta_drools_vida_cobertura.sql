﻿CREATE TABLE [bpm].[t_api_proposta_drools_vida_cobertura](
	[id_vida_cobertura] [int] IDENTITY(1,1) NOT NULL,
	[id_drools_vida] [int] NULL,
	[vl_capital] [numeric](19, 2) NULL,
	[nm_cobertura] [varchar](80) NULL,
 CONSTRAINT [PK_t_api_proposta_drools_vida_cobertura(id_vida_cobertura)] PRIMARY KEY CLUSTERED 
(
	[id_vida_cobertura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_drools_vida_cobertura]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_drools_vida_cobertura_X_t_api_proposta_drools_vida(id_drools_vida)] FOREIGN KEY([id_drools_vida])
REFERENCES [bpm].[t_api_proposta_drools_vida] ([id_drools_vida])
GO

ALTER TABLE [bpm].[t_api_proposta_drools_vida_cobertura] CHECK CONSTRAINT [FK_t_api_proposta_drools_vida_cobertura_X_t_api_proposta_drools_vida(id_drools_vida)]
GO