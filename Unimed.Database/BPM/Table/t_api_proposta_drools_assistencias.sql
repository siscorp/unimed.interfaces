﻿CREATE TABLE [bpm].[t_api_proposta_drools_assistencias](
	[id_drools_assistencias] [int] IDENTITY(1,1) NOT NULL,
	[id_proposta_drools] [int] NULL,
	[cd_assistencia] [int] NULL,
 CONSTRAINT [PK_t_api_proposta_drools_assistencias(id_drools_assistencias)] PRIMARY KEY CLUSTERED 
(
	[id_drools_assistencias] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_drools_assistencias]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_drools_assistencias_X_t_api_proposta_drools(id_proposta_drools)] FOREIGN KEY([id_proposta_drools])
REFERENCES [bpm].[t_api_proposta_drools] ([id_proposta_drools])
GO

ALTER TABLE [bpm].[t_api_proposta_drools_assistencias] CHECK CONSTRAINT [FK_t_api_proposta_drools_assistencias_X_t_api_proposta_drools(id_proposta_drools)]
GO