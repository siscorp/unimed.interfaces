﻿CREATE TABLE [bpm].[t_api_proposta_corretor_corretagem](
	[id_corretor_corretagem] [int] IDENTITY(1,1) NOT NULL,
	[id_proposta] [int] NULL,
	[cd_vendas] [int] NULL,
	[cd_relacionamento] [int] NULL,
	[cd_agente] [int] NULL,
	[cd_corretor] [int] NULL,
	[dv_principal] [bit] NULL,
	[cd_tp_comissao] [int] NULL,
	[nr_porcentagem] [int] NULL,
	[vl_comissao] [numeric](19, 2) NULL,
 CONSTRAINT [PK_t_api_proposta_corretor_corretagem(id_corretor_corretagem)] PRIMARY KEY CLUSTERED 
(
	[id_corretor_corretagem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_corretor_corretagem]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_corretor_corretagem_X_t_api_proposta(id_proposta)] FOREIGN KEY([id_proposta])
REFERENCES [bpm].[t_api_proposta] ([id_proposta])
GO

ALTER TABLE [bpm].[t_api_proposta_corretor_corretagem] CHECK CONSTRAINT [FK_t_api_proposta_corretor_corretagem_X_t_api_proposta(id_proposta)]
GO