﻿CREATE TABLE [bpm].[t_api_proposta](
	[id_proposta] [int] IDENTITY(1,1) NOT NULL,
	[nr_proposta] [int] NULL,
	[cd_cotacao] [int] NULL,
	[dt_competencia] [varchar](50) NULL,
	[dt_vigencia] [varchar](50) NULL,
	[cd_canal_venda] [int] NULL,
	[nm_cliente] [varchar](150) NULL,
	[nm_cpf] [varchar](20) NULL,
	[dt_nascimento] [smalldatetime] NULL,
	[dv_pe] [bit] NULL,
	[nm_tp_documento] [varchar](80) NULL,
	[nm_nr_documento] [varchar](50) NULL,
	[nm_orgao_emissor] [varchar](20) NULL,
	[dt_expedicao] [smalldatetime] NULL,
	[cd_sexo] [int] NULL,
	[cd_estado_civil] [int] NULL,
	[cd_relacionamento_unimed] [int] NULL,
	[cd_profissao] [int] NULL,
	[nm_ocupacao] [varchar](150) NULL,
	[cd_natureza_operacao] [int] NULL,
	[nm_email] [varchar](150) NULL,
	[vl_renda_mensal] [numeric](19, 2) NULL,
	[nm_score_health] [varchar](50) NULL,
	[cd_score_health] [int] NULL,
	[cd_conjuge] [int] NULL,
	[nm_conjuge] [varchar](150) NULL,
	[nm_cpf_conjuge] [varchar](20) NULL,
	[dt_nascimento_conjuge] [smalldatetime] NULL,
	[nm_tp_documento_conjuge] [varchar](80) NULL,
	[nm_nr_documento_conjuge] [varchar](50) NULL,
	[nm_orgao_emissor_conjuge] [varchar](20) NULL,
	[dt_expedicao_conjuge] [smalldatetime] NULL,
	[cd_cobertura_conjuge] [int] NULL,
	[vl_premio_total] [numeric](19, 2) NULL,
	[vl_premio_total_seguro] [numeric](19, 2) NULL,
	[cd_tipo_cobranca] [int] NULL,
	[dv_boleto_email] [bit] NULL,
	[nm_nosso_numero] [varchar](50) NULL,
	[cd_banco] [int] NULL,
	[cd_agencia] [int] NULL,
	[nr_conta] [int] NULL,
	[nr_digito_conta] [int] NULL,
	[cd_periodicidade] [int] NULL,
	[nr_dia_vencimento] [int] NULL,
	[nr_parcelas] [int] NULL,
	[nm_credit_card_number] [varchar](100) NULL,
	[nm_credit_card_adquirente] [varchar](100) NULL,
	[nm_credit_card_cliente] [varchar](100) NULL,
	[nm_credit_card_id_pedido] [varchar](100) NULL,
	[nm_credit_card_cd_pedido] [varchar](100) NULL,
	[nm_credit_card_transacao] [varchar](100) NULL,
	[nm_credit_card_token] [varchar](100) NULL,
 CONSTRAINT [PK_t_api_proposta(id_proposta)] PRIMARY KEY CLUSTERED 
(
	[id_proposta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO