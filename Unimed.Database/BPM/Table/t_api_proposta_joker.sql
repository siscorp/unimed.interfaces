﻿CREATE TABLE [bpm].[t_api_proposta_joker](
	[id_joker] [int] IDENTITY(1,1) NOT NULL,
	[id_proposta] [int] NULL,
	[nm_task] [varchar](200) NULL,
 CONSTRAINT [PK_t_api_proposta_joker(id_joker)] PRIMARY KEY CLUSTERED 
(
	[id_joker] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_api_proposta_joker]  WITH CHECK ADD  CONSTRAINT [FK_t_api_proposta_joker_X_t_api_proposta(id_proposta)] FOREIGN KEY([id_proposta])
REFERENCES [bpm].[t_api_proposta] ([id_proposta])
GO

ALTER TABLE [bpm].[t_api_proposta_joker] CHECK CONSTRAINT [FK_t_api_proposta_joker_X_t_api_proposta(id_proposta)]
GO