﻿CREATE TABLE [bpm].[t_bpm_parametro](
	[id_parametro] [int] IDENTITY(1,1) NOT NULL,
	[cd_parametro] [smallint] NOT NULL,
	[cd_recurso] [tinyint] NOT NULL,
	[nm_parametro] [varchar](80) NOT NULL,
	[vl_parametro] [varchar](1000) NULL,
 CONSTRAINT [pk_t_bpm_parametro(id_parametro)] PRIMARY KEY CLUSTERED 
(
	[id_parametro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [idx_t_bpm_parametro(cd_parametro$Cd_recurso)] UNIQUE NONCLUSTERED 
(
	[cd_parametro] ASC,
	[cd_recurso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [bpm].[t_bpm_parametro]  WITH CHECK ADD  CONSTRAINT [fk_t_bpm_parametro_X_t_bpm(cd_recurso)] FOREIGN KEY([cd_recurso])
REFERENCES [bpm].[t_bpm] ([cd_recurso])
GO

ALTER TABLE [bpm].[t_bpm_parametro] CHECK CONSTRAINT [fk_t_bpm_parametro_X_t_bpm(cd_recurso)]
GO

