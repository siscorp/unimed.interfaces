GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE SEGCORP
GO


		DECLARE @nr_versao_controle VARCHAR(20),
				@dt_versao_controle VARCHAR(50)
		


------ VERS�O DA TELA ---------------------------------------------
SELECT  @nr_versao_controle = '$Revision: 1.2.2.1 $',
				@dt_versao_controle = '$Date: 2020/11/12 18:54:15 $'
-------------------------------------------------------------------

DECLARE @cd_dina int = 11127
BEGIN TRANSACTION
BEGIN TRY
SET NOCOUNT ON



		IF ( replace(replace(@nr_versao_controle,'v.',''),'a','') > (select max(replace(replace(nr_versao,'v.',''),'a','')) FROM dina WHERE cd_dina = 11127))
		begin

			INSERT INTO dina_tela_log (cd_dina, nr_versao, nm_observacao, cd_usuario, dt_inclusao_log, cd_tipo_comando, nm_dina, nm_titulo, nm_tabela) 
			SELECT
			cd_dina,
			@nr_versao_controle, 
			'A00376795', 
			'T02696',
			'2022-02-09 10:30:40.987',
			'1',
			nm_dina,
			nm_titulo,
			nm_tabela
			FROM dina
			WHERE cd_dina = 11127

		end
		

		IF OBJECT_ID('tempdb..#dina') IS NOT NULL DROP TABLE #dina
		IF OBJECT_ID('tempdb..#dina_acao') IS NOT NULL DROP TABLE #dina_acao
		IF OBJECT_ID('tempdb..#dina_acao_parametro') IS NOT NULL DROP TABLE #dina_acao_parametro
		IF OBJECT_ID('tempdb..#dina_idioma_acao') IS NOT NULL DROP TABLE #dina_idioma_acao
		IF OBJECT_ID('tempdb..#dina_perfil_acao') IS NOT NULL DROP TABLE #dina_perfil_acao
		IF OBJECT_ID('tempdb..#dina_usuario_acao') IS NOT NULL DROP TABLE #dina_usuario_acao
		IF OBJECT_ID('tempdb..#dina_coluna_condicao_acao') IS NOT NULL DROP TABLE #dina_coluna_condicao_acao
		IF OBJECT_ID('tempdb..#dina_estatistica') IS NOT NULL DROP TABLE #dina_estatistica
		IF OBJECT_ID('tempdb..#dina_coluna') IS NOT NULL DROP TABLE #dina_coluna
		IF OBJECT_ID('tempdb..#dina_coluna_condicao_coluna') IS NOT NULL DROP TABLE #dina_coluna_condicao_coluna
		IF OBJECT_ID('tempdb..#dina_idioma_coluna') IS NOT NULL DROP TABLE #dina_idioma_coluna
		IF OBJECT_ID('tempdb..#dina_coluna_condicao') IS NOT NULL DROP TABLE #dina_coluna_condicao
		IF OBJECT_ID('tempdb..#dina_idioma_dina') IS NOT NULL DROP TABLE #dina_idioma_dina
		IF OBJECT_ID('tempdb..#dina_log') IS NOT NULL DROP TABLE #dina_log
		IF OBJECT_ID('tempdb..#dina_menu') IS NOT NULL DROP TABLE #dina_menu
		IF OBJECT_ID('tempdb..#dina_idioma_menu') IS NOT NULL DROP TABLE #dina_idioma_menu
		IF OBJECT_ID('tempdb..#dina_perfil_menu') IS NOT NULL DROP TABLE #dina_perfil_menu
		IF OBJECT_ID('tempdb..#dina_usuario_menu') IS NOT NULL DROP TABLE #dina_usuario_menu
		IF OBJECT_ID('tempdb..#dina_help_telas') IS NOT NULL DROP TABLE #dina_help_telas
CREATE TABLE #dina
		(
			cd_dina int ,
			nm_dina varchar(50),
			cd_tipo_tela int,
			nm_titulo varchar(50),
			nm_tabela varchar(255) ,
			cd_tipo_comando int ,
			nm_orderby varchar(100) ,
			nm_where varchar(300) ,
			nr_registro_pagina int ,
			dv_modo char(1) ,
			Cd_sistema int ,
			nm_query text ,
			nr_auto_refresh smallint ,
			dv_sem_link_lista bit ,
			cd_usuario varchar(10) ,
			dv_alterando bit ,
			dv_checkout bit ,
			id_usuario_checkout varchar(10) ,
			dt_checkout smalldatetime ,
			nm_set varchar(255) ,
			dv_traduz_conteudo bit
		)
CREATE TABLE #dina_acao(
			cd_acao int   ,
			cd_dina int  ,
			dv_log_sistema bit ,
			nm_acao varchar(25) ,
			cd_tipo_implementacao smallint ,
			nm_implementacao varchar(100) ,
			nm_implementacao_parametro varchar(800) ,
			cd_tipo_evento smallint ,
			cd_relatorio int ,
			nm_hint varchar(150) ,
			dv_modo char(1) ,
			cd_barra smallint ,
			cd_layout int ,
			nm_mensagem_alerta varchar(200) ,
			dv_padrao bit ,
			nm_target varchar(10) ,
			nm_servidor_remoto varchar(20) ,
			nr_ordem smallint ,
			nr_auto_refresh int ,
			id_dominio int ,
			dv_lock_app_resource bit,
			cd_acao_new int
		)
CREATE TABLE #dina_acao_parametro(
			id_parametro int  ,
			cd_acao int  ,
			nm_parametro varchar(40)
		)
CREATE TABLE #dina_idioma_acao(
			cd_idioma_acao int   ,
			cd_acao int  ,
			cd_idioma int ,
			nm_acao varchar(25) ,
			nm_hint varchar(150) ,
			nm_mensagem_alerta varchar(200) 
		)
CREATE TABLE #dina_perfil_acao(
			cd_perfil int  ,
			cd_acao int  ,
			dv_acesso bit 
		)
CREATE TABLE #dina_usuario_acao(
			cd_acao int  ,
			Cd_usuario varchar(10)  ,
			Dv_acesso bit 
		)
CREATE TABLE #dina_coluna(
			cd_coluna int   ,
			cd_lookup_dina int ,
			cd_funcao_validacao int ,
			cd_dina int  ,
			nm_campo varchar(50)  ,
			nr_ordem smallint ,
			nm_titulo varchar(50)  ,
			dv_mostra_lista char(1) ,
			dv_mostra_ficha char(1) ,
			dv_mostra_filtro char(1) ,
			dv_chave char(1) ,
			dv_somente_leitura char(1) ,
			nm_hint varchar(300) ,
			nm_lookup_tabela text ,
			nm_lookup_chave varchar(50) ,
			nm_lookup_lista varchar(50) ,
			nm_lookup_where varchar(400) ,
			dv_obrigatorio char(1) ,
			dv_mostra char(1) ,
			nm_formato varchar(30) ,
			nm_alias varchar(200) ,
			dv_lookup_estilo char(1) ,
			nm_classe_lista varchar(10) ,
			dv_edita_lista bit ,
			cd_valor_padrao int ,
			nm_campo_hint varchar(50) ,
			dv_traduz_conteudo bit,
			cd_coluna_new int
		)
CREATE TABLE #dina_estatistica(
			id_estatistica int  ,
			cd_dina int ,
			cd_acao int ,
			cd_coluna int ,
			cd_relatorio int ,
			dt_inicio_execucao datetime ,
			dt_fim_execucao datetime ,
			nm_metodo_origem varchar(100) ,
			nm_query_executada varchar(5000) ,
			nr_io int ,
			cd_sistema int ,
			dt_estatistica smalldatetime 
		)
CREATE TABLE #dina_idioma_coluna(
			cd_idioma_coluna int  ,
			cd_coluna int  ,
			cd_idioma int ,
			nm_titulo varchar(50) ,
			nm_hint varchar(300) 
		)
CREATE TABLE #dina_coluna_condicao(
			id_coluna_condicao int ,
			cd_coluna int  ,
			vl_campo varchar(80) ,
			nm_hint varchar(255) ,
			id_coluna_condicao_new int
		)
CREATE TABLE #dina_coluna_condicao_acao(
			id_coluna_mostra_acao int   ,
			id_coluna_condicao int ,
			cd_acao int  ,
			dv_mostra bit 
		)
CREATE TABLE #dina_coluna_condicao_coluna(
			id_coluna_mostra_condicao int  ,
			id_coluna_condicao int ,
			cd_coluna int  ,
			dv_mostra bit ,
			dv_protegido bit 
		)
CREATE TABLE #dina_idioma_dina(
			cd_idioma_dina int ,
			cd_dina int  ,
			cd_idioma int ,
			nm_titulo varchar(50) 
		)
CREATE TABLE #dina_log(
			id_log int   ,
			cd_usuario varchar(10) ,
			cd_dina int ,
			nm_data_action varchar(50) ,
			dt_erro smalldatetime ,
			nm_descricao varchar(8000) 
		)
CREATE TABLE #dina_menu(
			cd_menu int  ,
			cd_dina int ,
			Cd_empresa int ,
			Nm_menu varchar(25) ,
			Cd_sistema int ,
			Cd_menu_pai int
		)
CREATE TABLE #dina_idioma_menu(
			cd_idioma_menu int   ,
			cd_menu int ,
			cd_idioma int ,
			nm_menu varchar(25) 
		)
CREATE TABLE #dina_perfil_menu(
			cd_perfil int  ,
			cd_menu int  
		)
CREATE TABLE #dina_usuario_menu(
			Cd_usuario varchar(10)  ,
			cd_menu int  
		)
CREATE TABLE #dina_help_telas(
			id_dina_help_telas int   ,
			id_dina_help int ,
			cd_dina int 
		)

			delete from dina_help_telas
			where cd_dina = @cd_dina


				delete dum from dina_usuario_menu dum
				join dina_menu dm on dm.cd_menu = dum.cd_menu where cd_dina = @cd_dina

				

				if 2 = 3
				begin
					delete dpm from dina_perfil_menu dpm
					join dina_menu dm on dm.cd_menu = dpm.cd_menu where cd_dina = @cd_dina

				end

				delete dim from dina_idioma_menu dim
				join dina_menu dm on dm.cd_menu = dim.cd_menu where cd_dina = @cd_dina


			delete from dina_menu
			where cd_dina = @cd_dina


			delete from dina_idioma_dina
			where cd_dina = @cd_dina


			delete from dina_estatistica
			where cd_dina = @cd_dina


					delete dccc from dina_coluna_condicao_coluna dccc
					where dccc.id_coluna_condicao in (select id_coluna_condicao from dina_coluna_condicao where cd_coluna in (select cd_coluna from dina_coluna where cd_dina = @cd_dina)) and dccc.id_coluna_condicao is not null


					delete dcca from dina_coluna_condicao_acao dcca
					where dcca.id_coluna_condicao in (select id_coluna_condicao from dina_coluna_condicao where cd_coluna in (select cd_coluna from dina_coluna where cd_dina = @cd_dina)) and dcca.id_coluna_condicao is not null


				delete dcc from dina_coluna_condicao dcc
				where dcc.cd_coluna in (select cd_coluna from dina_coluna where cd_dina = @cd_dina)


				delete dic from dina_idioma_coluna dic
				join dina_coluna dc on dc.cd_coluna = dic.cd_coluna where cd_dina = @cd_dina


				delete de from dina_estatistica de
				join dina_coluna dc on dc.cd_coluna = de.cd_coluna where dc.cd_dina = @cd_dina


				delete dccc from dina_coluna_condicao_coluna dccc
				where dccc.cd_coluna in (select cd_coluna from dina_coluna where cd_dina = @cd_dina) --and dccc.id_coluna_condicao is null


				delete de from dina_estatistica de
				join dina_acao da on da.cd_acao = de.cd_acao where da.cd_dina = @cd_dina


				delete dcca from dina_coluna_condicao_acao dcca
				join dina_acao da on da.cd_acao = dcca.cd_acao where cd_dina = @cd_dina


				delete dua from dina_usuario_acao dua
				join dina_acao da on da.cd_acao = dua.cd_acao where cd_dina = @cd_dina


				

				if 2 = 3
				begin
					delete dpa from dina_perfil_acao dpa
					join dina_acao da on da.cd_acao = dpa.cd_acao where cd_dina = @cd_dina

				end

				delete dia from dina_idioma_acao dia
				join dina_acao da on da.cd_acao = dia.cd_acao where cd_dina = @cd_dina


				delete dap from dina_acao_parametro dap
				join dina_acao da on da.cd_acao = dap.cd_acao where cd_dina = @cd_dina


			delete from dina_coluna
			where cd_dina = @cd_dina


			delete from dina_acao
			where cd_dina = @cd_dina


		ALTER TABLE dina_coluna NOCHECK CONSTRAINT ALL 
		ALTER TABLE dina_log NOCHECK CONSTRAINT ALL

		 delete from dina
		 where cd_dina = @cd_dina

		ALTER TABLE dina_coluna WITH NOCHECK CHECK CONSTRAINT ALL 
		ALTER TABLE dina_log WITH NOCHECK CHECK CONSTRAINT ALL
		
--DINA--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

			set identity_insert dina on
			insert into #dina (cd_dina, nm_dina, cd_tipo_tela, nm_titulo, nm_tabela, cd_tipo_comando, nm_orderby, nm_where, nr_registro_pagina, dv_modo, Cd_sistema, nm_query, nr_auto_refresh, dv_sem_link_lista, cd_usuario, dv_alterando, dv_checkout, id_usuario_checkout, dt_checkout, nm_set, dv_traduz_conteudo)
			select 11127, 'TBpmParametro', 1, 'BPM - Parametros', 'bpm.t_bpm_parametro', 3, 'tbp.id_parametro', NULL, 10, 'L', NULL, 'select 
	tbp.id_parametro,
	tbp.cd_parametro,
	tbp.cd_recurso,
	tbp.nm_parametro,
	tbp.vl_parametro 
from bpm.t_bpm_parametro tbp', NULL, 0, 'T02696', 1, NULL, NULL, NULL, 'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; SET NOCOUNT ON;', NULL
			set identity_insert dina off
------DINA_ACAO--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				insert into #dina_acao (cd_acao, cd_dina, dv_log_sistema, nm_acao, cd_tipo_implementacao, nm_implementacao, nm_implementacao_parametro, cd_tipo_evento, cd_relatorio, nm_hint, dv_modo, cd_barra, cd_layout, nm_mensagem_alerta, dv_padrao, nm_target, nm_servidor_remoto, nr_ordem, nr_auto_refresh, id_dominio, dv_lock_app_resource)
				select 39427 ,11127 ,NULL, 'Pesquisar', 6, 'ConfirmaFicha', 'pesquisar', 1, NULL, 'Pesquisar por registros', 'A', 3, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 0

				insert into #dina_acao (cd_acao, cd_dina, dv_log_sistema, nm_acao, cd_tipo_implementacao, nm_implementacao, nm_implementacao_parametro, cd_tipo_evento, cd_relatorio, nm_hint, dv_modo, cd_barra, cd_layout, nm_mensagem_alerta, dv_padrao, nm_target, nm_servidor_remoto, nr_ordem, nr_auto_refresh, id_dominio, dv_lock_app_resource)
				select 39429 ,11127 ,NULL, 'Navega��o', 6, 'ConfirmaFicha', 'Navegacao', 1, NULL, 'Exibir barra de navega��o', 'A', 2, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 0
----------DINA_ACAO_PARAMETRO--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------DINA_IDIOMA_ACAO--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------DINA_PERFIL_ACAO--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------DINA_USUARIO_ACAO--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------DINA_ESTATISTICA--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------DINA_COLUNA--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				insert into #dina_coluna (cd_coluna, cd_lookup_dina, cd_funcao_validacao, cd_dina, nm_campo, nr_ordem, nm_titulo, dv_mostra_lista, dv_mostra_ficha, dv_mostra_filtro, dv_chave, dv_somente_leitura, nm_hint, nm_lookup_tabela, nm_lookup_chave, nm_lookup_lista, nm_lookup_where, dv_obrigatorio, dv_mostra, nm_formato, nm_alias, dv_lookup_estilo, nm_classe_lista, dv_edita_lista, cd_valor_padrao, nm_campo_hint, dv_traduz_conteudo)
				select 69509, NULL, NULL, 11127 ,'id_parametro', 0, '#C�digo', 'S', 'S', 'S', 'S', 'S', 'C�digo do Par�metro', NULL, NULL, NULL, NULL, 'S', 'N', NULL, 'tbp.id_parametro', NULL, NULL, 0, NULL, NULL, NULL

				insert into #dina_coluna (cd_coluna, cd_lookup_dina, cd_funcao_validacao, cd_dina, nm_campo, nr_ordem, nm_titulo, dv_mostra_lista, dv_mostra_ficha, dv_mostra_filtro, dv_chave, dv_somente_leitura, nm_hint, nm_lookup_tabela, nm_lookup_chave, nm_lookup_lista, nm_lookup_where, dv_obrigatorio, dv_mostra, nm_formato, nm_alias, dv_lookup_estilo, nm_classe_lista, dv_edita_lista, cd_valor_padrao, nm_campo_hint, dv_traduz_conteudo)
				select 69510, NULL, NULL, 11127 ,'cd_parametro', 1, '#C�digo Par�metro', 'S', 'S', 'S', 'N', 'S', 'C�digo do Par�metro', NULL, NULL, NULL, NULL, NULL, 'S', NULL, 'tbp.cd_parametro', 'N', NULL, 0, NULL, NULL, NULL

				insert into #dina_coluna (cd_coluna, cd_lookup_dina, cd_funcao_validacao, cd_dina, nm_campo, nr_ordem, nm_titulo, dv_mostra_lista, dv_mostra_ficha, dv_mostra_filtro, dv_chave, dv_somente_leitura, nm_hint, nm_lookup_tabela, nm_lookup_chave, nm_lookup_lista, nm_lookup_where, dv_obrigatorio, dv_mostra, nm_formato, nm_alias, dv_lookup_estilo, nm_classe_lista, dv_edita_lista, cd_valor_padrao, nm_campo_hint, dv_traduz_conteudo)
				select 69511, NULL, NULL, 11127 ,'cd_recurso', 2, '#C�digo Recurso', 'S', 'S', 'S', 'N', 'S', 'C�digo do Recurso', NULL, NULL, NULL, NULL, NULL, 'N', NULL, 'tbp.cd_recurso', NULL, NULL, 0, NULL, NULL, NULL

				insert into #dina_coluna (cd_coluna, cd_lookup_dina, cd_funcao_validacao, cd_dina, nm_campo, nr_ordem, nm_titulo, dv_mostra_lista, dv_mostra_ficha, dv_mostra_filtro, dv_chave, dv_somente_leitura, nm_hint, nm_lookup_tabela, nm_lookup_chave, nm_lookup_lista, nm_lookup_where, dv_obrigatorio, dv_mostra, nm_formato, nm_alias, dv_lookup_estilo, nm_classe_lista, dv_edita_lista, cd_valor_padrao, nm_campo_hint, dv_traduz_conteudo)
				select 69512, NULL, NULL, 11127 ,'nm_parametro', 3, 'Par�metro', 'S', 'S', 'S', 'N', 'S', 'Descri��o do Par�metro', NULL, NULL, NULL, NULL, NULL, 'S', NULL, 'tbp.nm_parametro', NULL, NULL, 0, NULL, NULL, NULL

				insert into #dina_coluna (cd_coluna, cd_lookup_dina, cd_funcao_validacao, cd_dina, nm_campo, nr_ordem, nm_titulo, dv_mostra_lista, dv_mostra_ficha, dv_mostra_filtro, dv_chave, dv_somente_leitura, nm_hint, nm_lookup_tabela, nm_lookup_chave, nm_lookup_lista, nm_lookup_where, dv_obrigatorio, dv_mostra, nm_formato, nm_alias, dv_lookup_estilo, nm_classe_lista, dv_edita_lista, cd_valor_padrao, nm_campo_hint, dv_traduz_conteudo)
				select 69513, NULL, NULL, 11127 ,'vl_parametro', 4, 'Valor de Par�metro', 'S', 'S', 'S', 'N', 'N', 'Valor de referencia do Par�metro', NULL, NULL, NULL, NULL, NULL, 'S', NULL, 'tbp.vl_parametro', NULL, NULL, 0, NULL, NULL, NULL
----------DINA_COLUNA_CONDICAO_COLUNA--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------DINA_ESTATISTICA--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------DINA_IDIOMA_COLUNA--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------DINA_COLUNA_CONDICAO--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------DINA_COLUNA_CONDICAO_ACAO--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------DINA_COLUNA_CONDICAO_COLUNA--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------DINA_ESTATISTICA--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------DINA_IDIOMA_DINA--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------DINA_MENU--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------DINA_IDIOMA_MENU--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------DINA_PERFIL_MENU--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------DINA_USUARIO_MENU--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------DINA_HELP_TELAS--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		set identity_insert dina on
		insert into dina (cd_dina, nm_dina, cd_tipo_tela, nm_titulo, nm_tabela, cd_tipo_comando, nm_orderby, nm_where, nr_registro_pagina, dv_modo, Cd_sistema, nm_query, nr_auto_refresh, dv_sem_link_lista, cd_usuario, dv_alterando, dv_checkout, id_usuario_checkout, dt_checkout, nm_set, dv_traduz_conteudo)
		select dt.cd_dina, dt.nm_dina, dt.cd_tipo_tela, dt.nm_titulo, dt.nm_tabela, dt.cd_tipo_comando, dt.nm_orderby, dt.nm_where, dt.nr_registro_pagina, dt.dv_modo, dt.Cd_sistema, dt.nm_query, dt.nr_auto_refresh, dt.dv_sem_link_lista, dt.cd_usuario, dt.dv_alterando, dt.dv_checkout, dt.id_usuario_checkout, dt.dt_checkout, dt.nm_set, dt.dv_traduz_conteudo
		from #dina dt
		where dt.cd_dina = @cd_dina
		set identity_insert dina off
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

			insert into dina_acao (cd_dina, dv_log_sistema, nm_acao, cd_tipo_implementacao, nm_implementacao, nm_implementacao_parametro, cd_tipo_evento, cd_relatorio, nm_hint, dv_modo, cd_barra, cd_layout, nm_mensagem_alerta, dv_padrao, nm_target, nm_servidor_remoto, nr_ordem, nr_auto_refresh, id_dominio, dv_lock_app_resource)
			select dat.cd_dina, dat.dv_log_sistema, dat.nm_acao, dat.cd_tipo_implementacao, dat.nm_implementacao, dat.nm_implementacao_parametro, dat.cd_tipo_evento, dat.cd_relatorio, dat.nm_hint, dat.dv_modo, dat.cd_barra, dat.cd_layout, dat.nm_mensagem_alerta, dat.dv_padrao, dat.nm_target, dat.nm_servidor_remoto, dat.nr_ordem, dat.nr_auto_refresh, dat.id_dominio, dat.dv_lock_app_resource
			from #dina_acao dat
			where dat.cd_dina = @cd_dina and cd_acao = 39427
			update #dina_acao set cd_acao_new = @@IDENTITY where cd_acao = 39427

			insert into dina_acao (cd_dina, dv_log_sistema, nm_acao, cd_tipo_implementacao, nm_implementacao, nm_implementacao_parametro, cd_tipo_evento, cd_relatorio, nm_hint, dv_modo, cd_barra, cd_layout, nm_mensagem_alerta, dv_padrao, nm_target, nm_servidor_remoto, nr_ordem, nr_auto_refresh, id_dominio, dv_lock_app_resource)
			select dat.cd_dina, dat.dv_log_sistema, dat.nm_acao, dat.cd_tipo_implementacao, dat.nm_implementacao, dat.nm_implementacao_parametro, dat.cd_tipo_evento, dat.cd_relatorio, dat.nm_hint, dat.dv_modo, dat.cd_barra, dat.cd_layout, dat.nm_mensagem_alerta, dat.dv_padrao, dat.nm_target, dat.nm_servidor_remoto, dat.nr_ordem, dat.nr_auto_refresh, dat.id_dominio, dat.dv_lock_app_resource
			from #dina_acao dat
			where dat.cd_dina = @cd_dina and cd_acao = 39429
			update #dina_acao set cd_acao_new = @@IDENTITY where cd_acao = 39429
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				insert into dina_acao_parametro (cd_acao, nm_parametro)
				select dat.cd_acao_new, dapt.nm_parametro
				from #dina_acao_parametro dapt
				join #dina_acao dat on dat.cd_acao = dapt.cd_acao and dat.cd_dina = @cd_dina
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				insert into dina_idioma_acao (cd_acao, cd_idioma, nm_acao, nm_hint, nm_mensagem_alerta)
				select dat.cd_acao_new, diat.cd_idioma, diat.nm_acao, diat.nm_hint, diat.nm_mensagem_alerta
				from #dina_idioma_acao diat
				join #dina_acao dat on dat.cd_acao = diat.cd_acao and dat.cd_dina = @cd_dina
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				insert into dina_perfil_acao (cd_perfil, cd_acao, dv_acesso)
				select dpat.cd_perfil, dat.cd_acao_new, dpat.dv_acesso
				from #dina_perfil_acao dpat
				join #dina_acao dat 
				on dat.cd_acao = dpat.cd_acao 
				and dat.cd_dina = @cd_dina
				left join dina_perfil_acao dpac
				on dpac.cd_perfil = dpat.cd_perfil
				and dpac.cd_acao = dat.cd_acao_new
				where dpac.cd_perfil is null
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				insert into dina_usuario_acao (cd_acao, Cd_usuario, Dv_acesso)
				select dat.cd_acao_new, duat.Cd_usuario, duat.Dv_acesso
				from #dina_usuario_acao duat
				join #dina_acao dat on dat.cd_acao = duat.cd_acao and dat.cd_dina = @cd_dina
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				insert into dina_estatistica (cd_dina, cd_acao, cd_coluna, cd_relatorio, dt_inicio_execucao, dt_fim_execucao, nm_metodo_origem, nm_query_executada, nr_io, cd_sistema, dt_estatistica)
				select det.cd_dina, dat.cd_acao_new, det.cd_coluna, det.cd_relatorio, det.dt_inicio_execucao, det.dt_fim_execucao, det.nm_metodo_origem, det.nm_query_executada, det.nr_io, det.cd_sistema, det.dt_estatistica
				from #dina_estatistica det
				join #dina_acao dat on dat.cd_acao = det.cd_acao and det.cd_dina = @cd_dina
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

			insert into dina_coluna (cd_lookup_dina, cd_funcao_validacao, cd_dina, nm_campo, nr_ordem, nm_titulo, dv_mostra_lista, dv_mostra_ficha, dv_mostra_filtro, dv_chave, dv_somente_leitura, nm_hint, nm_lookup_tabela, nm_lookup_chave, nm_lookup_lista, nm_lookup_where, dv_obrigatorio, dv_mostra, nm_formato, nm_alias, dv_lookup_estilo, nm_classe_lista, dv_edita_lista, cd_valor_padrao, nm_campo_hint, dv_traduz_conteudo)
			select dct.cd_lookup_dina, dct.cd_funcao_validacao, dct.cd_dina, dct.nm_campo, dct.nr_ordem, dct.nm_titulo, dct.dv_mostra_lista, dct.dv_mostra_ficha, dct.dv_mostra_filtro, dct.dv_chave, dct.dv_somente_leitura, dct.nm_hint, dct.nm_lookup_tabela, dct.nm_lookup_chave, dct.nm_lookup_lista, dct.nm_lookup_where, dct.dv_obrigatorio, dct.dv_mostra, dct.nm_formato, dct.nm_alias, dct.dv_lookup_estilo, dct.nm_classe_lista, dct.dv_edita_lista, dct.cd_valor_padrao, dct.nm_campo_hint, dct.dv_traduz_conteudo
			from #dina_coluna dct
			where dct.cd_dina = @cd_dina and cd_coluna = 69509
			update #dina_coluna set cd_coluna_new = @@IDENTITY where cd_coluna = 69509

			insert into dina_coluna (cd_lookup_dina, cd_funcao_validacao, cd_dina, nm_campo, nr_ordem, nm_titulo, dv_mostra_lista, dv_mostra_ficha, dv_mostra_filtro, dv_chave, dv_somente_leitura, nm_hint, nm_lookup_tabela, nm_lookup_chave, nm_lookup_lista, nm_lookup_where, dv_obrigatorio, dv_mostra, nm_formato, nm_alias, dv_lookup_estilo, nm_classe_lista, dv_edita_lista, cd_valor_padrao, nm_campo_hint, dv_traduz_conteudo)
			select dct.cd_lookup_dina, dct.cd_funcao_validacao, dct.cd_dina, dct.nm_campo, dct.nr_ordem, dct.nm_titulo, dct.dv_mostra_lista, dct.dv_mostra_ficha, dct.dv_mostra_filtro, dct.dv_chave, dct.dv_somente_leitura, dct.nm_hint, dct.nm_lookup_tabela, dct.nm_lookup_chave, dct.nm_lookup_lista, dct.nm_lookup_where, dct.dv_obrigatorio, dct.dv_mostra, dct.nm_formato, dct.nm_alias, dct.dv_lookup_estilo, dct.nm_classe_lista, dct.dv_edita_lista, dct.cd_valor_padrao, dct.nm_campo_hint, dct.dv_traduz_conteudo
			from #dina_coluna dct
			where dct.cd_dina = @cd_dina and cd_coluna = 69510
			update #dina_coluna set cd_coluna_new = @@IDENTITY where cd_coluna = 69510

			insert into dina_coluna (cd_lookup_dina, cd_funcao_validacao, cd_dina, nm_campo, nr_ordem, nm_titulo, dv_mostra_lista, dv_mostra_ficha, dv_mostra_filtro, dv_chave, dv_somente_leitura, nm_hint, nm_lookup_tabela, nm_lookup_chave, nm_lookup_lista, nm_lookup_where, dv_obrigatorio, dv_mostra, nm_formato, nm_alias, dv_lookup_estilo, nm_classe_lista, dv_edita_lista, cd_valor_padrao, nm_campo_hint, dv_traduz_conteudo)
			select dct.cd_lookup_dina, dct.cd_funcao_validacao, dct.cd_dina, dct.nm_campo, dct.nr_ordem, dct.nm_titulo, dct.dv_mostra_lista, dct.dv_mostra_ficha, dct.dv_mostra_filtro, dct.dv_chave, dct.dv_somente_leitura, dct.nm_hint, dct.nm_lookup_tabela, dct.nm_lookup_chave, dct.nm_lookup_lista, dct.nm_lookup_where, dct.dv_obrigatorio, dct.dv_mostra, dct.nm_formato, dct.nm_alias, dct.dv_lookup_estilo, dct.nm_classe_lista, dct.dv_edita_lista, dct.cd_valor_padrao, dct.nm_campo_hint, dct.dv_traduz_conteudo
			from #dina_coluna dct
			where dct.cd_dina = @cd_dina and cd_coluna = 69511
			update #dina_coluna set cd_coluna_new = @@IDENTITY where cd_coluna = 69511

			insert into dina_coluna (cd_lookup_dina, cd_funcao_validacao, cd_dina, nm_campo, nr_ordem, nm_titulo, dv_mostra_lista, dv_mostra_ficha, dv_mostra_filtro, dv_chave, dv_somente_leitura, nm_hint, nm_lookup_tabela, nm_lookup_chave, nm_lookup_lista, nm_lookup_where, dv_obrigatorio, dv_mostra, nm_formato, nm_alias, dv_lookup_estilo, nm_classe_lista, dv_edita_lista, cd_valor_padrao, nm_campo_hint, dv_traduz_conteudo)
			select dct.cd_lookup_dina, dct.cd_funcao_validacao, dct.cd_dina, dct.nm_campo, dct.nr_ordem, dct.nm_titulo, dct.dv_mostra_lista, dct.dv_mostra_ficha, dct.dv_mostra_filtro, dct.dv_chave, dct.dv_somente_leitura, dct.nm_hint, dct.nm_lookup_tabela, dct.nm_lookup_chave, dct.nm_lookup_lista, dct.nm_lookup_where, dct.dv_obrigatorio, dct.dv_mostra, dct.nm_formato, dct.nm_alias, dct.dv_lookup_estilo, dct.nm_classe_lista, dct.dv_edita_lista, dct.cd_valor_padrao, dct.nm_campo_hint, dct.dv_traduz_conteudo
			from #dina_coluna dct
			where dct.cd_dina = @cd_dina and cd_coluna = 69512
			update #dina_coluna set cd_coluna_new = @@IDENTITY where cd_coluna = 69512

			insert into dina_coluna (cd_lookup_dina, cd_funcao_validacao, cd_dina, nm_campo, nr_ordem, nm_titulo, dv_mostra_lista, dv_mostra_ficha, dv_mostra_filtro, dv_chave, dv_somente_leitura, nm_hint, nm_lookup_tabela, nm_lookup_chave, nm_lookup_lista, nm_lookup_where, dv_obrigatorio, dv_mostra, nm_formato, nm_alias, dv_lookup_estilo, nm_classe_lista, dv_edita_lista, cd_valor_padrao, nm_campo_hint, dv_traduz_conteudo)
			select dct.cd_lookup_dina, dct.cd_funcao_validacao, dct.cd_dina, dct.nm_campo, dct.nr_ordem, dct.nm_titulo, dct.dv_mostra_lista, dct.dv_mostra_ficha, dct.dv_mostra_filtro, dct.dv_chave, dct.dv_somente_leitura, dct.nm_hint, dct.nm_lookup_tabela, dct.nm_lookup_chave, dct.nm_lookup_lista, dct.nm_lookup_where, dct.dv_obrigatorio, dct.dv_mostra, dct.nm_formato, dct.nm_alias, dct.dv_lookup_estilo, dct.nm_classe_lista, dct.dv_edita_lista, dct.cd_valor_padrao, dct.nm_campo_hint, dct.dv_traduz_conteudo
			from #dina_coluna dct
			where dct.cd_dina = @cd_dina and cd_coluna = 69513
			update #dina_coluna set cd_coluna_new = @@IDENTITY where cd_coluna = 69513
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				insert into dina_coluna_condicao_coluna (id_coluna_condicao, cd_coluna, dv_mostra, dv_protegido)
				select dccct.id_coluna_condicao, dct.cd_coluna_new, dccct.dv_mostra, dccct.dv_protegido
				from #dina_coluna_condicao_coluna dccct
				join #dina_coluna dct on dct.cd_coluna = dccct.cd_coluna and dccct.id_coluna_condicao is null
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				insert into dina_estatistica (cd_dina, cd_acao, cd_coluna, cd_relatorio, dt_inicio_execucao, dt_fim_execucao, nm_metodo_origem, nm_query_executada, nr_io, cd_sistema, dt_estatistica)
				select det.cd_dina, det.cd_acao, dct.cd_coluna_new, det.cd_relatorio, det.dt_inicio_execucao, det.dt_fim_execucao, det.nm_metodo_origem, det.nm_query_executada, det.nr_io, det.cd_sistema, det.dt_estatistica
				from #dina_estatistica det
				join #dina_coluna dct on dct.cd_coluna = det.cd_coluna
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				insert into dina_idioma_coluna (cd_coluna, cd_idioma, nm_titulo, nm_hint)
				select dct.cd_coluna_new, dict.cd_idioma, dict.nm_titulo, dict.nm_hint
				from #dina_idioma_coluna dict
				join #dina_coluna dct on dct.cd_coluna = dict.cd_coluna
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					insert into dina_coluna_condicao_acao (id_coluna_condicao, cd_acao, dv_mostra)
					select dcct.id_coluna_condicao_new, dat.cd_acao_new, dccat.dv_mostra
					from #dina_coluna_condicao_acao dccat
					join #dina_coluna_condicao dcct on dcct.id_coluna_condicao = dccat.id_coluna_condicao and dccat.id_coluna_condicao is not null
					join #dina_acao dat on dccat.cd_acao = dat.cd_acao
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					insert into dina_coluna_condicao_coluna (id_coluna_condicao, cd_coluna, dv_mostra, dv_protegido)
					select dcct.id_coluna_condicao_new, dct.cd_coluna_new, dccct.dv_mostra, dccct.dv_protegido
					from #dina_coluna_condicao_coluna dccct
					join #dina_coluna_condicao dcct on dcct.id_coluna_condicao = dccct.id_coluna_condicao and dccct.id_coluna_condicao is not null
					join #dina_coluna dct on dct.cd_coluna = dccct.cd_coluna and dct.cd_dina = @cd_dina
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

			insert into dina_estatistica (cd_dina, cd_acao, cd_coluna, cd_relatorio, dt_inicio_execucao, dt_fim_execucao, nm_metodo_origem, nm_query_executada, nr_io, cd_sistema, dt_estatistica)
			select det.cd_dina, det.cd_acao, det.cd_coluna, det.cd_relatorio, det.dt_inicio_execucao, det.dt_fim_execucao, det.nm_metodo_origem, det.nm_query_executada, det.nr_io, det.cd_sistema, det.dt_estatistica
			from #dina_estatistica det
			where det.cd_dina = @cd_dina and det.cd_coluna is null and det.cd_acao is null
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

			insert into dina_idioma_dina (cd_dina, cd_idioma, nm_titulo)
			select didt.cd_dina, didt.cd_idioma, didt.nm_titulo
			from #dina_idioma_dina didt
			join dina din on din.cd_dina = @cd_dina and didt.cd_dina = @cd_dina
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

			insert into dina_menu (cd_menu, cd_dina, Cd_empresa, Nm_menu, Cd_sistema, Cd_menu_pai)
			select dmt.cd_menu, dmt.cd_dina, dmt.Cd_empresa, dmt.Nm_menu, dmt.Cd_sistema, dmt.Cd_menu_pai
			from #dina_menu dmt
			where dmt.cd_dina = @cd_dina
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				insert into dina_idioma_menu (cd_menu, cd_idioma, nm_menu)
				select dimt.cd_menu, dimt.cd_idioma, dimt.nm_menu
				from #dina_idioma_menu dimt
				join #dina_menu dmt on dmt.cd_menu = dimt.cd_menu
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				insert into dina_perfil_menu (cd_perfil, cd_menu)
				select dpmt.cd_perfil, dpmt.cd_menu
				from #dina_perfil_menu dpmt
				join #dina_menu dmt 
				on dmt.cd_menu = dpmt.cd_menu
				left join dina_perfil_menu cptm
				on cptm.cd_perfil = dpmt.cd_perfil
				and cptm.cd_menu = dpmt.cd_menu
				where cptm.cd_perfil is null
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				insert into dina_usuario_menu (Cd_usuario, cd_menu)
				select dumt.Cd_usuario, dumt.cd_menu
				from #dina_usuario_menu dumt
				join #dina_menu dmt on dmt.cd_menu = dumt.cd_menu
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

			insert into dina_help_telas (id_dina_help, cd_dina)
			select dhtt.id_dina_help, dhtt.cd_dina
			from #dina_help_telas dhtt
			where dhtt.cd_dina = @cd_dina
			
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SET NOCOUNT OFF
		SELECT 'SCRIPT EXECUTOU COM SUCESSO!'
		
select '#dina_acao (cd_acao_new)', count(*) as contador from dina_acao da where da.cd_dina = @cd_dina

		select '#dina_acao_parametro (cd_acao)', count(*) as contador from dina_acao_parametro dap
		where dap.cd_acao in (select cd_acao from dina_acao where cd_dina = @cd_dina)

		select '#dina_idioma_acao(cd_acao)', count(*) as contador from dina_idioma_acao dia
		where dia.cd_acao in (select cd_acao from dina_acao where cd_dina = @cd_dina)

		
		------neto ( ignorar ) 
		select '#dina_perfil_acao(cd_acao)', count(*) as contador from dina_perfil_acao dpa
		where dpa.cd_acao in (select cd_acao from dina_acao where cd_dina = @cd_dina)

		select '#dina_usuario_acao (cd_acao)', count(*) as contador from dina_usuario_acao dua
		where dua.cd_acao in (select cd_acao from dina_acao where cd_dina = @cd_dina)

		select '#dina_coluna (cd_coluna_new)', count(*) as contador from dina_coluna dc where dc.cd_dina = @cd_dina

		select '#dina_coluna_condicao_coluna(cd_coluna,id_coluna_condicao)', count(*) as contador from dina_coluna_condicao_coluna dccc
		where dccc.cd_coluna in (select cd_coluna from dina_coluna where cd_dina = @cd_dina) and dccc.id_coluna_condicao is null

		select '#dina_estatistica(cd_coluna)', count(*) as contador from dina_estatistica de
		where de.cd_coluna in (select cd_coluna from dina_coluna where cd_dina = @cd_dina) and de.cd_dina is null

		select '#dina_idioma_coluna(cd_coluna)', count(*) as contador from dina_idioma_coluna dic
		where dic.cd_coluna in (select cd_coluna from dina_coluna where cd_dina = @cd_dina)

		select '#dina_coluna_condicao(cd_coluna, id_coluna_condicao_new)', count(*) as contador from dina_coluna_condicao dcc
		where dcc.cd_coluna in (select cd_coluna from dina_coluna where cd_dina = @cd_dina)

		select '#dina_coluna_condicao_acao(id_coluna_condicao, cd_acao)', count(*) as contador from dina_coluna_condicao_acao dcca
		where dcca.id_coluna_condicao in (select id_coluna_condicao from dina_coluna_condicao where cd_coluna in (select cd_coluna from dina_coluna where cd_dina = @cd_dina)) and dcca.id_coluna_condicao is not null

		select '#dina_coluna_condicao_coluna(id_coluna_condicao, cd_coluna)', count(*) as contador from dina_coluna_condicao_coluna dccc
		where dccc.id_coluna_condicao in (select id_coluna_condicao from dina_coluna_condicao where cd_coluna in (select cd_coluna from dina_coluna where cd_dina = @cd_dina)) and dccc.id_coluna_condicao is not null

		select '#dina_menu(cd_menu_new)', count(*) as contador from dina_menu dm where dm.cd_dina = @cd_dina

		select '#dina_idioma_menu(cd_menu)', count(*) as contador from dina_idioma_menu dim
		where dim.cd_menu in (select cd_menu from dina_menu where cd_dina = @cd_dina)

		
		-----neto (ignorar) 
		select '#dina_perfil_menu(cd_menu)', count(*) as contador from dina_perfil_menu dpm
		where dpm.cd_menu in (select cd_menu from dina_menu where cd_dina = @cd_dina)

		select '#dina_usuario_menu(cd_menu)', count(*) as contador from dina_usuario_menu dum
		where dum.cd_menu in (select cd_menu from dina_menu where cd_dina = @cd_dina)

		--In�cio dos casos especiais
		

		--Final dos casos especiais
		

		UPDATE dina SET nr_versao = @nr_versao_controle WHERE cd_dina =11127

		COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
		SELECT 'SCRIPT EXECUTOU COM FALHA: ' + ERROR_MESSAGE() AS ErrorMessage
		ROLLBACK TRANSACTION
		END CATCH


Hor�rio de conclus�o: 2022-02-09T10:30:41.2536808-03:00
