﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Unimed.Shared.IO
{
    internal class FileDir
    {
        public enum tp_tratamento
        {
            GED,
            Outros
        }
        public static String CriaPasta(String nm_diretorio, String nm_pasta_atual, tp_tratamento tratamento)
        {
            String nr_proxima_pasta = String.Empty;
            if (tratamento == tp_tratamento.GED)
            {
                nr_proxima_pasta = Path.Combine(nm_diretorio, (Convert.ToInt32(nm_pasta_atual) + 1).ToString());
                CriaPasta(nr_proxima_pasta);
            }
            return nr_proxima_pasta;
        }
        public static void CriaPasta(String nm_diretorio)
        {
            try
            {
                if (!Directory.Exists(nm_diretorio))
                    Directory.CreateDirectory(nm_diretorio);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
