﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;

namespace Unimed.Shared.DAL
{
    public class p_recurso_parametro
    {
        public List<EN.p_recurso_parametro> Executar(SqlConnection conn, NameValueCollection objRequest)
        {
            List<EN.p_recurso_parametro> nm_retorno = new List<EN.p_recurso_parametro>();

            try
            {
                Unimed.Shared.DATA.ExecutaProcedures exec = new Shared.DATA.ExecutaProcedures();
                exec.cdRetornoThrowErro = true;
                exec.ThrowErro = true;

                using (SqlDataReader dr = exec.ExecuteReader(conn, "[dbo].[p_recurso_parametro]", objRequest))
                {
                    if (!dr.HasRows)
                        throw new Exception("Parametro(s) não encontrado(s).");

                    if (dr.Read())
                    {
                        do
                        {
                            nm_retorno.Add(new EN.p_recurso_parametro()
                            {
                                id_parametro = Convert.ToInt16(dr["id_parametro"]),
                                cd_parametro = Convert.ToInt16(dr["cd_parametro"]),
                                id_recurso = Convert.ToInt16(dr["id_recurso"]),
                                nm_parametro = Convert.ToString(dr["nm_parametro"]),
                                nm_valor = Convert.ToString((dr["nm_valor"]))
                            });
                        }
                        while (dr.Read());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return nm_retorno;

        }
    }
}
