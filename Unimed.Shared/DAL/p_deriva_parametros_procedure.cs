﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Unimed.Shared.DAL
{
    public class p_deriva_parametros_procedure
    {
        public static void DeriveParameters(SqlCommand cmd, Boolean dv_null = false )
        {
            try
            {
                SqlCommandBuilder.DeriveParameters(cmd);
            }
            catch (Exception ex)
            {
                SqlDataAdapter da = new SqlDataAdapter(
                    @"select" + "\r\n" +
                    "\t" + @"p.name," + "\r\n" +
                    "\t" + @"Type = t.name," + "\r\n" +
                    "\t" + @"p.max_length," + "\r\n" +
                    "\t" + @"p.precision," + "\r\n" +
                    "\t" + @"p.scale," + "\r\n" +
                    "\t" + @"p.is_output" + "\r\n" +
                    @"from sys.parameters p" + "\r\n" +
                    "\t" + @"join sys.objects o on" + "\r\n" +
                    "\t" + @String.Empty + "\t" + @"p.object_id = o.object_id" + "\r\n" +
                    "\t" + @"join sys.types t on" + "\r\n" +
                    "\t" + @String.Empty + "\t" + @"p.user_type_id = t.user_type_id" + "\r\n" +
                    @"where" + "\r\n" +
                    "\t" + @"o.name = @objname" + "\r\n" +
                    @"order by p.parameter_id", cmd.Connection);
                da.SelectCommand.Parameters.Add("@objname", SqlDbType.VarChar).Value = cmd.CommandText;

                try
                {
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    SqlParameter p;
                    foreach (DataRow row in dt.Rows)
                    {
                        p = new SqlParameter(row["name"].ToString(),
                           Util.Parametros.ObtemTipoColuna(row["Type"].ToString()),
                            Util.Parametros.ObtemTipoColuna(row["Type"].ToString()) == SqlDbType.NVarChar && Convert.ToInt32(row["max_length"].ToString()) == 8000 ? -1 : Convert.ToInt32(row["max_length"].ToString()));
                        p.Precision = Convert.ToByte(row["precision"].ToString());
                        p.Scale = Convert.ToByte(row["scale"].ToString());
                        if (Convert.Equals(row["is_output"], true)) p.Direction = ParameterDirection.InputOutput;

                        cmd.Parameters.Add(p);
                    }
                    p = null;

                    dt.Dispose();
                    da.Dispose();
                }
                catch (Exception ex2)
                {
                    throw new ApplicationException("Ocorreram os seguintes erros ao derivar os parâmetros da procedure: " + cmd.CommandText +
                        "\r\n1: " + ex.Message +
                        "\r\n2: " + ex2.Message);
                }
            }
        }

        internal static List<EN.p_deriva_parametros_procedure> DeriveParameters(SqlCommand cmd)
        {
            List<EN.p_deriva_parametros_procedure> colecaoParametros = new List<EN.p_deriva_parametros_procedure>();

            try
            {

                EN.p_deriva_parametros_procedure obj = null;

                /* Obtem os parametros da Procedure  */
                SqlCommandBuilder.DeriveParameters(cmd);

                /* Percorre os campos para obter os parametros */
                foreach (SqlParameter param in cmd.Parameters)
                {
                    if ((param.Direction != ParameterDirection.Input && param.Direction != ParameterDirection.InputOutput) || (param.ParameterName.Substring(1).ToLower().Equals("cd_retorno") || param.ParameterName.Substring(1).ToLower().Equals("nm_retorno")))
                        continue;

                    obj = new EN.p_deriva_parametros_procedure();

                    obj.nm_campo = param.ParameterName.Substring(1);
                    obj.nm_type = param.SqlDbType.ToString();
                    obj.nm_tipo_parametro = param.Direction;

                    colecaoParametros.Add(obj);
                }

            }
            catch (SqlException sql_ex)
            {
                throw new Exception(String.Format("Erro de Banco de Dados ao obter parammetros dos Procedure '{0}', erro: {1}", cmd.CommandText.ToString(), sql_ex.InnerException.GetBaseException()));
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Erro ao obter os parametros da Procedure '{0}', erro: {1}", cmd.CommandText.ToString(), ex.GetBaseException()));
            }

            return colecaoParametros;
        }
    }
}
