﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Unimed.Shared.EN
{
    internal class p_deriva_parametros_procedure
    {
        private Boolean _dv_possui_valor;
        public Boolean dv_possui_valor
        {
            get { return _dv_possui_valor; }
            set { _dv_possui_valor = value; }
        }

        private string _nm_campo;
        public string nm_campo
        {
            get { return _nm_campo; }
            set { _nm_campo = value; }
        }

        private string _vl_campo;
        public string vl_campo
        {
            get { return _vl_campo; }
            set { _vl_campo = value; }
        }

        private Object _nm_type;
        public Object nm_type
        {
            get { return _nm_type; }
            set { _nm_type = value; }
        }

        private ParameterDirection _nm_tipo_parametro;
        public ParameterDirection nm_tipo_parametro
        {
            get { return _nm_tipo_parametro; }
            set { _nm_tipo_parametro = value; }
        }

        private System.Data.SqlClient.SqlParameter _arr_parametros;
        public System.Data.SqlClient.SqlParameter arr_parametros
        {
            get { return _arr_parametros; }
            set { _arr_parametros = value; }
        }

    }
}
