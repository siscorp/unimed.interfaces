﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unimed.Shared.EN
{
    public class p_recurso_parametro
    {
        private Int16 _id_parametro;
        private Int16 _cd_parametro;
        private Int16 _id_recurso;
        private String _nm_parametro;
        private String _nm_valor;

        public short id_parametro { get => _id_parametro; set => _id_parametro = value; }
        public short cd_parametro { get => _cd_parametro; set => _cd_parametro = value; }
        public short id_recurso { get => _id_recurso; set => _id_recurso = value; }
        public string nm_parametro { get => _nm_parametro; set => _nm_parametro = value; }
        public string nm_valor { get => _nm_valor; set => _nm_valor = value; }

    }
}
