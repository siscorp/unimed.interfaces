﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;

namespace Unimed.Shared.Util
{
    public class Parametros : SortedDictionary<string, object>
    {

        #region Construtora

        internal bool SequencialKey { get; set; }
        public Parametros() : base(StringComparer.CurrentCultureIgnoreCase)
        {
            SequencialKey = false;
        }

        /// <summary>
        /// Cria collection a partir de um NameValueCollection
        /// </summary>
        /// <param name="nvc"></param>
        public Parametros(NameValueCollection nvc) : this()
        {
            foreach (string key in nvc.Keys)
            {
                if (!String.IsNullOrEmpty(nvc[key]))
                    this[key] = nvc[key];
            }
        }

        /// <summary>
        /// Cria collection a partir de uma Page
        /// </summary>
        /// <param name="nvc"></param>
        public Parametros(System.Web.HttpContext context) : this()
        {
            // Todos os parâmetros
            foreach (String key in context.Request.Params)
            {
                if (context.Request.Params[key] == null)
                    continue;
                // Filtro padrão
                if (key.Equals("FiltroPadrao"))
                    this.InsertOrUpdate(context.Request["FiltroPadrao"].ToString());
                else
                    this[key] = context.Request.Params[key];
            }

            foreach (String key in context.Session.Keys)
                this[key] = context.Session[key];            
        }

        #endregion

        #region Inclusão básica

        /// <summary>
        /// Adiciona um novo par key / value. 
        /// Gera exception se já existir
        /// </summary>
        /// <param name="key">Chave (se não tiver @ será adicionado)</param>
        /// <param name="value">Valor</param>
        public new void Add(string key, object value)
        {
            base.Add(FormatKey(key), value);
        }


        /// <summary>
        /// Set - altera um par key / value ou cria um novo se não existir
        /// Get - pega um par key / value ou gera exception se não existir
        /// </summary>
        /// <param name="key">Chave (se tiver @ será removido)</param>
        /// <returns></returns>
        public new object this[string key]
        {
            get
            {
                try
                {
                    return base[FormatKey(key)];
                }
                catch (Exception ex)
                {
                    throw new Exception("Chave '" + key + "' não encontrada na Parametros. ", ex);
                }

            }

            set
            {
                base[FormatKey(key)] = value;
            }
        }

        #endregion

        #region Inclusão de outras extruturas

        /// <summary>
        /// Adiciona as chaves de uma outra coleção nesta
        /// Se o par já existir sobrepoe
        /// </summary>
        public void InsertOrUpdate(Parametros outra)
        {
            if (outra == null)
                return;

            foreach (var campo in outra)
                this[campo.Key] = campo.Value;
        }

        /// <summary>
        /// Adiciona as colunas / valores de um DataRow como Key / Value
        /// Se o par já existir sobrepoe
        /// </summary>
        /// <param name="dr">DataRow</param>
        public void InsertOrUpdate(DataRow dr)
        {
            if (dr == null)
                return;

            for (Int32 i = 0; i < dr.Table.Columns.Count; i++)
                this[dr.Table.Columns[i].ColumnName] = dr[i] == System.DBNull.Value ? null : dr[i];
        }

        /// <summary>
        /// Adiciona as colunas / valores de um DataRow como Key / Value se o valor não for null
        /// Se o par já existir sobrepoe
        /// </summary>
        /// <param name="dr">DataRow</param>
        public void InsertOrUpdateNotNull(DataRow dr)
        {
            if (dr == null)
                return;

            for (Int32 i = 0; i < dr.Table.Columns.Count; i++)
            {
                if (dr[i] != System.DBNull.Value && dr[i] != null)
                    this[dr.Table.Columns[i].ColumnName] = dr[i];
            }
        }


        /// <summary>
        /// Adiciona as propriedades de um determinado objeto como key e respectivos valores como value
        /// Se o par já existir sobrepoe
        /// </summary>
        /// <param name="tp">Class com propriedades a adicionar na coleção</param>
        public void InsertOrUpdate(object obj)
        {
            InsertOrUpdate(obj, "");
        }

        /// <summary>
        /// Adiciona as propriedades de um determinado objeto como key e respectivos valores como value
        /// O nome da propriedade é prefixado.
        /// Se o par já existir sobrepoe
        /// </summary>
        /// <param name="obj">Class com propriedades a adicionar na coleção</param>
        /// <param name="prefixo">Prefixo que será colocado na frete do nome da propriedade para compor a key da coleção</param>
        public void InsertOrUpdate(object obj, string prefixo)
        {
            if (obj == null)
                return;

            PropertyInfo[] props = obj.GetType().GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object valor = prop.GetValue(obj, null);

                if (valor != null)
                    this[prefixo + prop.Name] = valor;
            }
        }

        /// <summary>
        /// Adiciona os campos e valores de uma string da forma: campo1=valor1|and|campo2=valor2|and|campo3=valor3...
        /// Se um par já existir sobrepoe
        /// </summary>
        /// <param name="tp">Class com propriedades a adicionar na coleção</param>
        public void InsertOrUpdate(string filtroPadrao)
        {
            if (Helper.IsNullOrEmptyOrSpace(filtroPadrao))
                return;

            string[] paramFiltroPadrao = filtroPadrao.Split(new String[] { "|and|" }, StringSplitOptions.None);

            foreach (string param in paramFiltroPadrao)
            {
                try
                {
                    string[] campoValor = param.Split('=');

                    if (campoValor.Length == 2 && !Helper.IsNullOrEmptyOrSpace(campoValor[0]) && !Helper.IsNullOrEmptyOrSpace((String.IsNullOrEmpty(campoValor[1]) ? null : (campoValor[1].ToString().Equals("null") ? null : campoValor[1]))))
                    {
                        if (campoValor[1].StartsWith("\'"))
                        {
                            DateTime? valor = Helper.ConverteData(campoValor[1]);

                            if (valor != null)
                                this[campoValor[0]] = valor.Value;
                            else
                                this[campoValor[0]] = campoValor[1];
                        }
                        else
                            this[campoValor[0]] = campoValor[1];
                    }
                }
                catch
                {

                }

            }
        }

        /// <summary>
        /// Insere a partir de uma NameValueCollection acertando para null campos dt e dv
        /// </summary>
        public void InsertOrUpdate(NameValueCollection pObjRequest)
        {
            foreach (string key in pObjRequest.Keys)
            {
                switch (key.Substring(0, 3))
                {
                    case "dt_":
                        if (pObjRequest[key] == null || Helper.IsNullOrEmptyOrSpace(pObjRequest[key].ToString()))
                            this[key] = null;
                        else
                            this[key] = pObjRequest[key];
                        break;

                    case "dv_":
                        if (pObjRequest[key] == null || Helper.IsNullOrEmptyOrSpace(pObjRequest[key].ToString()))
                            this[key] = null;
                        else
                            this[key] = pObjRequest[key].ToString().Trim().Equals("0") ? 0 : 1;
                        break;

                    default:
                        this[key] = pObjRequest[key];
                        break;
                }
            }
        }


        #endregion

        #region Extração básica

        /// <summary>
        /// Retorna um item da coleção em um determinado tipo
        /// </summary>
        public T GetItem<T>(string key)
        {
            object valor = this[key];

            try
            {
                if (valor is T)
                    return (T)valor;

                Type t = typeof(T);
                t = Nullable.GetUnderlyingType(t) ?? t;

                return (valor == null) ? default(T) : (T)System.Convert.ChangeType(valor, t);
            }
            catch (Exception ex)
            {
                throw new Exception("Impossivel converter '" + valor.ToString() + "' extraido da Parametros['" + key + "'] para " + typeof(T) + ". ", ex);
            }

        }

        /// <summary>
        /// Retorna um item da coleção em um determinado tipo ou caso a chave não exista retorna o valor default
        /// </summary>
        public T GetItemOrDefault<T>(string key, T valorDefault)
        {
            if (!this.ContainsKey(FormatKey(key)))
                return valorDefault;

            return GetItem<T>(key);
        }

        #endregion

        #region Extração para outras extruturas

        /// <summary>
        /// Monta uma NameValueCollection a partir da coleção interna
        /// </summary>
        public NameValueCollection GetNameValueCollection()
        {
            NameValueCollection nvc = new NameValueCollection();
            return GetNameValueCollection(nvc);
        }

        /// <summary>
        /// Popula uma NameValueCollection existente com os itens da coleção interna
        /// </summary>
        public NameValueCollection GetNameValueCollection(NameValueCollection nvc)
        {
            foreach (string key in this.Keys)
            {
                if (this[key] != null)
                    nvc.Add(key, this[key].ToString());
            }

            return nvc;
        }

        #endregion

        #region Auxiliares

        /// <summary>
        /// Garante que key não tenha @ no inicio
        /// </summary>
        internal static string FormatKey(string key)
        {
            if (Helper.IsNullOrEmptyOrSpace(key))
                throw new Exception("Erro em Parametros chave não pode ser nula.");

            if (key.StartsWith("@"))
                key = key.Substring(1);

            return key;
        }

        #endregion


        #region "Outros"
        public static SqlDbType ObtemTipoColuna(String tipoSql)
        {
            switch (tipoSql.ToLower())
            {
                case "bigint":
                    return SqlDbType.BigInt;
                case "binary":
                    return SqlDbType.Binary;
                case "bit":
                    return SqlDbType.Bit;
                case "char":
                    return SqlDbType.Char;
                case "datetime":
                    return SqlDbType.DateTime;
                case "decimal":
                    return SqlDbType.Decimal;
                case "float":
                    return SqlDbType.Float;
                case "int":
                    return SqlDbType.Int;
                case "image":
                    return SqlDbType.Image;
                case "money":
                    return SqlDbType.Money;
                case "nchar":
                    return SqlDbType.NChar;
                case "ntext":
                    return SqlDbType.NText;
                case "nvarchar":
                    return SqlDbType.NVarChar;
                case "real":
                    return SqlDbType.Real;
                case "smalldatetime":
                    return SqlDbType.SmallDateTime;
                case "smallint":
                    return SqlDbType.SmallInt;
                case "smallmoney":
                    return SqlDbType.SmallMoney;
                case "text":
                    return SqlDbType.Text;
                case "timestmap":
                    return SqlDbType.Timestamp;
                case "tinyint":
                    return SqlDbType.TinyInt;
                case "varbinary":
                    return SqlDbType.VarBinary;
                case "varchar":
                    return SqlDbType.VarChar;
                default:
                    return SqlDbType.VarChar;
            }
        }
        public static Object DefinirValorParametro(DataColumn dc, Object Valor)
        {
            try
            {
                if (String.IsNullOrEmpty(Convert.ToString(Valor)))
                {
                    return "null";
                }

                switch (dc.DataType.Name.ToLower())
                {
                    case "string":
                        Valor = "'" + Valor.ToString().Replace("'", "''") + "'";
                        break;
                    case "boolean":
                        Valor = Valor.Equals(false) ? "0" : "1";
                        break;
                    case "datetime":
                        Valor = "'" + Util.Helper.FormataDataGravar(Valor.ToString()) + "'";
                        break;
                }

                return Valor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void DefinirValorParametro(SqlParameter parametro, Object Valor)
        {
            try
            {
                if (Convert.Equals(Valor, null) || Convert.Equals(Valor, DBNull.Value))
                {
                    parametro.Value = DBNull.Value;
                    return;
                }

                switch (parametro.SqlDbType)
                {
                    case SqlDbType.BigInt:
                    case SqlDbType.Decimal:
                    case SqlDbType.Float:
                    case SqlDbType.Int:
                    case SqlDbType.Money:
                    case SqlDbType.Real:
                    case SqlDbType.SmallInt:
                    case SqlDbType.SmallMoney:
                    case SqlDbType.TinyInt:
                        parametro.Value = Util.Helper.FormataNumeroGravar(Valor.ToString(), Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator, Thread.CurrentThread.CurrentCulture.NumberFormat.NumberGroupSeparator);
                        break;
                    case SqlDbType.Date:
                    case SqlDbType.DateTime:
                    case SqlDbType.DateTime2:
                    case SqlDbType.DateTimeOffset:
                    case SqlDbType.SmallDateTime:
                        parametro.Value = Util.Helper.FormataDataGravar(Valor.ToString());
                        break;
                    case SqlDbType.Bit:
                        parametro.Value = Valor.Equals(true) || Valor.ToString().Equals("True", StringComparison.OrdinalIgnoreCase) || Convert.ToBoolean(Convert.ToInt32(Valor));
                        break;
                    case SqlDbType.Image:
                    case SqlDbType.VarBinary:
                    case SqlDbType.Binary:
                        parametro.Value = Valor;
                        break;
                    default:
                        parametro.Value = Valor.ToString();
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
