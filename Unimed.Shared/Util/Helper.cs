﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Unimed.Shared.Util
{
    public class Helper
    {
        public static void Replace(StringBuilder Expression, String Find, String Replacement)
        {
            String aux = Expression.ToString();

            Expression.Remove(0, Expression.Length);
            Expression.Append(Replace(aux, Find, Replacement));
        }
        public static String Replace(String Expression, String Find, String Replacement)
        {
            CompareOptions ordinal;
            CompareInfo compareInfo;
            Int32 num5 = 0;
            Int32 length = Expression.Length;
            Int32 num2 = Find.Length;
            StringBuilder builder = new StringBuilder(length);
            Int32 Count = Expression.Length;

            try
            {
                compareInfo = CompareInfo.GetCompareInfo("pt-BR");
                ordinal = CompareOptions.IgnoreWidth | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreCase;

                while (num5 < length)
                {
                    Int32 num4 = 0;
                    if (num4 == Count)
                    {
                        builder.Append(Expression.Substring(num5));
                        break;
                    }
                    Int32 num3 = compareInfo.IndexOf(Expression, Find, num5, ordinal);
                    if (num3 < 0)
                    {
                        builder.Append(Expression.Substring(num5));
                        break;
                    }
                    builder.Append(Expression.Substring(num5, num3 - num5));
                    builder.Append(Replacement);
                    num4++;
                    num5 = num3 + num2;
                }
                return builder.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                builder = null;
            }
        }
        public static double ConvertBytesToMegabytes(Int32 bytes)
        {
            return Convert.ToDouble(((bytes / 1024f) / 1024f).ToString("N3"));
        }
        public static double ConvertKilobytesToMegabytes(Int64 kilobytes)
        {
            return kilobytes / 1024f;
        }
        public static String ConvertNull(Object Objeto)
        {
            if (Convert.Equals(Objeto, null))
                return String.Empty;
            else
                return Objeto.ToString();
        }
        public static Boolean IsNumeric(String Numero)
        {
            double x = 2;
            return Double.TryParse(Numero, out x);
        }
        public static String TrataData(String vl_data)
        {
            if (vl_data == "01/01/0001 00:00:00")
            {
                vl_data = string.Empty;
            }

            DateTime data;
            string[] formats = {"d/M/yyyy",
                                "d/M/yyyy h:mm:ss tt",
                                "d/M/yyyy h:mm tt",
                                "d/M/yyyy hh:mm tt",
                                "d/M/yyyy hh tt",
                                "d/M/yyyy h:mm",
                                "d/M/yyyy h:mm:ss",
                                "d/M/yyyy hh:mm",

                                "dd/MM/yyyy",
                                "dd/MM/yyyy hh:mm:ss",
                                "dd/MM/yyyy h:mm:ss tt",
                                "dd/MM/yyyy h:mm tt",
                                "dd/MM/yyyy hh:mm tt",
                                "dd/MM/yyyy hh tt",
                                "dd/MM/yyyy h:mm",
                                "dd/MM/yyyy h:mm:ss",
                                "dd/MM/yyyy HH:mm:ss",
                                "dd/MM/yyyy hh:mm",

                                "yyyy-MM-dd",
                                "yyyy-MM-dd h:mm:ss tt",
                                "yyyy-MM-dd h:mm tt",
                                "yyyy-MM-dd hh:mm tt",
                                "yyyy-MM-dd hh tt",
                                "yyyy-MM-dd h:mm",
                                "yyyy-MM-dd h:mm:ss",
                                "yyyy-MM-dd HH:mm:ss",
                                "yyyy-MM-dd hh:mm",

                                "yyyyMMdd",
                                "yyyyMMdd h:mm:ss tt",
                                "yyyyMMdd h:mm tt",
                                "yyyyMMdd hh:mm tt",
                                "yyyyMMdd hh tt",
                                "yyyyMMdd h:mm",
                                "yyyyMMdd h:mm:ss",
                                "yyyyMMdd hh:mm",
            };

            if (!DateTime.TryParseExact(vl_data, formats, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out data))
                return null;

            return data.ToString("yyyy-MM-dd HH:mm:ss");

        }
        public static String FormataDataGravar(String data)
        {
            if (String.IsNullOrEmpty(data))
                return String.Empty;

            data = Replace(data, "&nbsp;", " ");

            if (data.Length > 8)
            {
                return Convert.ToDateTime(data).ToString("yyyy-MM-dd HH:mm:ss");
            }
            else if (data.Length.Equals(8))
            {
                return Convert.ToDateTime(data.Substring(0, 4) + "-" + data.Substring(4, 2) + "-" + data.Substring(6, 2)).ToString("yyyy-MM-dd");
            }
            else
            {
                return data;
            }
        }
        public static Object FormataNumeroGravar(String Numero, String Decimal, String Milhar)
        {
            String sDecimal = Convert.ToDouble("1111,11").ToString("N").Substring(5, 1);

            if (String.IsNullOrEmpty(Numero))
                return null;

            String tmp = Numero;
            tmp = tmp.Replace(Decimal, "@");
            tmp = tmp.Replace(Milhar, String.Empty);
            tmp = tmp.Replace("@", sDecimal);

            return Convert.ToDecimal(tmp);
        }
        public static bool IsNullOrEmptyOrSpace(string str)
        {
            if (str == null || str.Trim().Length == 0)
                return true;

            return false;
        }
        public static bool IsNullOrEmptyOrSpace(Object objeto)
        {
            if (objeto is System.DBNull)
                return true;

            if (objeto == null)
                return true;

            if (objeto.ToString().Trim().Length == 0)
                return true;

            return false;
        }
        public static DateTime? ConverteData(String vl_data)
        {
            vl_data = TrataData(vl_data);
            if (vl_data != null)
                return DateTime.Parse(vl_data);
            else
                return null;
        }

        /// <summary>
        /// Carrega todos os campos de uma DataSet generica para a coleção de parametros
        /// </summary>
        /// <param name="parametros">Lista de parametros a ser incluido os dados</param>
        /// <param name="nm_param">Classe Generica</param>
        public static void ObtemParametrosSistema(NameValueCollection objRequest, System.Data.DataSet dataset, String coluna1, String coluna2)
        {
            /* Percorre as colunas */
            for (int indiceY = 0; indiceY < dataset.Tables[0].Rows.Count; indiceY++)
            {
                if (!String.IsNullOrEmpty(dataset.Tables[0].Rows[indiceY][coluna1].ToString()))
                {

                    ObtemParametrosSistema(objRequest, dataset.Tables[0].Rows[indiceY][coluna1].ToString(), dataset.Tables[0].Rows[indiceY][coluna2].ToString());

                }
            }

        }
        public static void ObtemParametrosSistema(NameValueCollection objRequest, NameValueCollection obj)
        {
            foreach (String par in obj)
                ObtemParametrosSistema(objRequest, par, obj[par]);
        }
        /// <summary>
        /// Carrega todos os campos de uma classe entity generica para a coleção de parametros
        /// </summary>
        /// <param name="parametros">Lista de parametros a ser incluido os dados</param>
        /// <param name="nm_param">Classe Generica</param>
        public static void ObtemParametrosSistema<T>(NameValueCollection objRequest, T classe)
        {
            foreach (var item in classe.GetType().GetProperties())
            {
                ObtemParametrosSistema(objRequest, item.Name, item.GetValue(classe, null));
            }
        }
        /// <summary>
        /// Carrega a coleção de parametros da consulta
        /// </summary>
        /// <param name="parametros">Lista de parametros a ser incluido os dados</param>
        /// <param name="nm_param">Nome do parametro a ser incluido</param>
        /// <param name="nm_texto">Dado a ser incluido</param>        
        public static void ObtemParametrosSistema(NameValueCollection objRequest, String nm_param, String nm_texto)
        {
            if (objRequest.AllKeys.Contains(nm_param))
                objRequest[nm_param] = nm_texto;
            else
                objRequest.Add(nm_param, nm_texto);
        }
        public static void ObtemParametrosSistemaSession(NameValueCollection objRequest, String nm_param, String nm_texto, String complemento_session)
        {
            if (objRequest.AllKeys.Contains(nm_param.ToLower().Replace(complemento_session.ToLower(), "")))
                objRequest.Remove(nm_param.ToLower().Replace(complemento_session.ToLower(), ""));

            objRequest.Add(nm_param.ToLower().Replace(complemento_session.ToLower(), ""), nm_texto);
        }
        public static void ObtemParametrosSistema(NameValueCollection objRequest, String nm_param, Object nm_texto)
        {
            ObtemParametrosSistema(objRequest, nm_param, Convert.ToString((nm_texto == null ? "" : nm_texto)));
        }
        public static void ObtemParametrosSistema(Util.Parametros parametros, String nm_param, Object vl_param)
        {
            if (parametros.ContainsKey(nm_param))
                parametros[nm_param] = vl_param;
            else
                parametros.Add(nm_param, vl_param);
        }
        /// <summary>
        /// Carrega na coleção de parametros os dados a linha do datareader
        /// </summary>
        /// <param name="objRequest">parametros</param>
        /// <param name="dr">DataReader</param>
        /// <param name="dv_fecha_reader">Verifica se deve fechar a conexão</param>
        public static void ObtemParametrosSistema(NameValueCollection objRequest, System.Data.SqlClient.SqlDataReader dr, Boolean dv_fecha_reader)
        {
            try
            {
                /* Percorre as colunas */
                for (int indiceY = 0; indiceY < dr.FieldCount; indiceY++)
                    if (dr.GetName(indiceY) != null)
                        if (dr.GetName(indiceY).ToString().Length > 0)
                            ObtemParametrosSistema(objRequest, dr.GetName(indiceY).ToString(), dr[indiceY].ToString());
            }
            finally
            {
                if (dv_fecha_reader)
                    if (dr.IsClosed == false)
                        dr.Close();
            }
        }
        //public static String ObtemParametrosSistema(Util.Parametros parametros, string[] args)
        //{
        //    /* Lista de Paramentros
        //     * -I = ID DA CONFIGURAÇÃO
        //     * -C = ID DA CONSULTA
        //     * -S = CONNECTIONSTRING,
        //     * -F = ID DA FILA
        //     * -E = E-MAIL USUÁRIO QUE INCLUIR O ITEM NA FILA
        //     * -N = NOME DO USUÁRIO QUE INCLUI O ITEM NA FILA
        //    */
        //    StringBuilder sLog = new StringBuilder();


        //    String valor = String.Empty;
        //    String ConnectionString = String.Empty;
        //    clCriptografia cp = new clCriptografia();

        //    foreach (String str in args)
        //    {

        //        if (str.StartsWith("-"))
        //        {

        //            if (char.ToUpper(str[1]) == 'I')
        //            {

        //                valor = str.Substring(2).Trim();
        //                ObtemParametrosSistema(parametros, "id_configuracao", valor);
        //            }
        //            else if (char.ToUpper(str[1]) == 'C')
        //            {
        //                valor = str.Substring(2).Trim();
        //                ObtemParametrosSistema(parametros, "cd_consulta", valor);
        //            }
        //            else if (char.ToUpper(str[1]) == 'S')
        //            {
        //                ConnectionString = cp.Descriptografar(str.Substring(2).Trim(), "I4PROINFO");
        //                ObtemParametrosSistema(parametros, "ConnectionString", ConnectionString);
        //            }
        //            else if (char.ToUpper(str[1]) == 'F')
        //            {
        //                valor = str.Substring(2).Trim();
        //                ObtemParametrosSistema(parametros, "id_fila", valor);
        //            }
        //            else if (char.ToUpper(str[1]) == 'E')
        //            {
        //                valor = str.Substring(2).Trim();
        //                ObtemParametrosSistema(parametros, "nm_email", valor);
        //            }
        //            else if (char.ToUpper(str[1]) == 'N')
        //            {
        //                valor = str.Substring(2).Trim();
        //                ObtemParametrosSistema(parametros, "nm_nome", valor);
        //            }
        //        }
        //    }
        //    //ConnectionString = @"data source=sqldev\sql2008r2;initial catalog=i4pro_edn_dev;integrated security=SSPI;trusted_connection=true;MultipleActiveResultSets=True";

        //    return ConnectionString;
        //}
        //public static void ObtemParametrosSistema(NameValueCollection objRequest, string[] args)
        //{
        //    /* Lista de Paramentros
        //     * -O = Lista de parametros
        //    */
        //    StringBuilder sLog = new StringBuilder();

        //    String valor = String.Empty;
        //    String ConnectionString = String.Empty;
        //    clCriptografia criptografia = new clCriptografia();

        //    foreach (String str in args)
        //    {

        //        if (str.StartsWith("-"))
        //        {
        //            if (char.ToUpper(str[1]) == 'O')
        //            {
        //                valor = str.Substring(2).Trim();
        //                ObtemParametrosSistema(objRequest, valor);
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Obtem parametros do sistema
        /// </summary>
        /// <param name="objRequest">Dicionario de dados</param>
        /// <param name="nm_consulta">Texto</param>
        /// <returns></returns>
        public static String ObtemParametrosSistema(NameValueCollection objRequest, String nm_consulta)
        {
            String acao = String.Empty;
            String key = String.Empty;

            if (nm_consulta.Contains("?"))
            {
                acao = nm_consulta.Substring(0, nm_consulta.IndexOf('?'));
                String comando = nm_consulta.Substring(nm_consulta.IndexOf('?') + 1);

                String[] comandos = comando.Split('&');

                String nome = String.Empty;
                String valor = String.Empty;

                // Hash parâmetros preenchido com parâmetros tipo QueryString, recebido como ação
                foreach (String parametro in comandos)
                {
                    if (!String.IsNullOrEmpty(parametro))
                    {
                        nome = parametro.Substring(0, parametro.IndexOf('='));
                        valor = parametro.Substring(parametro.IndexOf('=') + 1);
                        ObtemParametrosSistema(objRequest, nome, valor);
                    }
                }
            }
            else
            {
                ObtemParametrosSistema(objRequest, "nm_consulta", nm_consulta);
                acao = nm_consulta;
            }
            return acao;
        }
        /// <summary>
        /// Remonta a coleção de parametros
        /// </summary>
        /// <param name="objRequest"></param>
        /// <param name="NovoRequest"></param>
        /// <returns></returns>
        public static NameValueCollection ObtemParametrosSistema(NameValueCollection objRequest)
        {
            NameValueCollection NovoRequest = new NameValueCollection();
            for (Int32 indice = 0; indice < objRequest.Count; indice++)
                ObtemParametrosSistema(NovoRequest, objRequest.GetKey(indice), objRequest[indice]);

            return NovoRequest;
        }
        public static NameValueCollection ObtemParametrosSistemaSession(NameValueCollection objRequest, String complemento_Session)
        {
            NameValueCollection NovoRequest = new NameValueCollection();
            for (Int32 indice = 0; indice < objRequest.Count; indice++)
            {

                if (objRequest.GetKey(indice).ToLower().Contains(complemento_Session.ToLower()))
                    ObtemParametrosSistemaSession(NovoRequest, objRequest.GetKey(indice), objRequest[indice], complemento_Session);

                else if (NovoRequest[objRequest.GetKey(indice)] == null)
                    ObtemParametrosSistema(NovoRequest, objRequest.GetKey(indice), objRequest[indice]);
            }

            return NovoRequest;
        }
        public static void ObtemParametrosSistema(Dictionary<String, Object> dicJson, NameValueCollection parametros)
        {
            foreach (KeyValuePair<String, Object> item in dicJson)
            {
                ObtemParametrosSistema(parametros, item.Key, item.Value.ToString());
            }
        }
    }
}
