﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Unimed.Shared.DATA
{
    internal class p_colecao_parametros
    {
        public static void MontaColecao(List<EN.p_deriva_parametros_procedure> colecaoParametros, NameValueCollection objRequest)
        {

            for (int indice = 0; indice < colecaoParametros.Count; indice++)
            {
                foreach (string key in objRequest.Keys)
                {
                    if (key.Equals(colecaoParametros[indice].nm_campo, StringComparison.CurrentCultureIgnoreCase))
                    {
                        colecaoParametros[indice].dv_possui_valor = true;
                        colecaoParametros[indice].vl_campo = objRequest[key];
                    }
                }
            }
        }

        public static void Parametros(ref SqlCommand cmd, List<EN.p_deriva_parametros_procedure> colecaoParametros)
        {
            for (Int16 indice = 0; indice < colecaoParametros.Count; indice++)
            {
                if (colecaoParametros[indice].dv_possui_valor)
                {
                    if (!String.IsNullOrEmpty(colecaoParametros[indice].vl_campo))
                    {

                        if (colecaoParametros[indice].nm_type.ToString().ToLower().Contains("datetime"))
                            cmd.Parameters[String.Format("@{0}", colecaoParametros[indice].nm_campo)].Value = Util.Helper.TrataData(colecaoParametros[indice].vl_campo);
                        else if (colecaoParametros[indice].nm_type.ToString().ToLower().Contains("decimal"))
                        {
                            if (!colecaoParametros[indice].vl_campo.Contains(",") & colecaoParametros[indice].vl_campo.Contains("."))
                            {
                                cmd.Parameters[String.Format("@{0}", colecaoParametros[indice].nm_campo)].Value = Convert.ToDecimal(colecaoParametros[indice].vl_campo.Replace(".", ","));
                            }
                            else
                                cmd.Parameters[String.Format("@{0}", colecaoParametros[indice].nm_campo)].Value = Convert.ToDecimal(colecaoParametros[indice].vl_campo);                            
                        }
                        else if (colecaoParametros[indice].nm_type.ToString().ToLower().Contains("bit"))
                        {
                            if (colecaoParametros[indice].vl_campo.Equals("0") || colecaoParametros[indice].vl_campo.Equals("false") || colecaoParametros[indice].vl_campo.Equals("False"))
                                cmd.Parameters[String.Format("@{0}", colecaoParametros[indice].nm_campo)].Value = "false";
                            else
                                cmd.Parameters[String.Format("@{0}", colecaoParametros[indice].nm_campo)].Value = "true";
                        }
                        else if (colecaoParametros[indice].nm_type.ToString().ToLower().Contains("varbinary"))
                        {
                            cmd.Parameters[String.Format("@{0}", colecaoParametros[indice].nm_campo)].Value = Convert.FromBase64String(colecaoParametros[indice].vl_campo);
                        }
                        else
                            cmd.Parameters[String.Format("@{0}", colecaoParametros[indice].nm_campo)].Value = colecaoParametros[indice].vl_campo;
                    }
                    else
                        cmd.Parameters[(String.Format("@{0}", colecaoParametros[indice].nm_campo))].Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters[(String.Format("@{0}", colecaoParametros[indice].nm_campo))].Value = DBNull.Value;
                }
            }
        }

        public static void ParametrosOutPut(SqlCommand cmd, ref NameValueCollection objRequest, Boolean cdRetornoThrowErro, ref Int32 cd_retorno, ref String nm_retorno)
        {
            cd_retorno = 0;
            nm_retorno = String.Format("Procedure {0} gerou um erro não tratado.", cmd.CommandText.ToString());

            foreach (SqlParameter parametro in cmd.Parameters)
            {
                if (parametro.Direction == ParameterDirection.Output || parametro.Direction == ParameterDirection.InputOutput)
                {
                    string nmParametro = parametro.ParameterName;

                    if (nmParametro.StartsWith("@"))
                        nmParametro = nmParametro.Substring(1);

                    if (nmParametro.ToLower().Equals("cd_retorno") && (parametro.Value == null))
                        Util.Helper.ObtemParametrosSistema(objRequest, nmParametro, (parametro.Value == null ? "0" : parametro.Value));
                    else
                        Util.Helper.ObtemParametrosSistema(objRequest, nmParametro, (parametro.Value == DBNull.Value ? null : parametro.Value));

                    if (nmParametro.ToLower().Equals("cd_retorno") && (parametro.Value != null) && parametro.Value != System.DBNull.Value)
                        int.TryParse(parametro.Value.ToString(), out cd_retorno);


                    if (nmParametro.ToLower().Equals("nm_retorno") && (parametro.Value != null) && parametro.Value != System.DBNull.Value)
                        nm_retorno = parametro.Value.ToString();
                }
            }

            if (cdRetornoThrowErro && !cd_retorno.ToString().Equals("0"))
                throw new Exception(nm_retorno);
        }

        public static void ParametrosOutPut(SqlCommand cmd, ref Util.Parametros parametros, Boolean cdRetornoThrowErro, ref Int32 cd_retorno, ref String nm_retorno)
        {
            cd_retorno = 0;
            nm_retorno = String.Format("Procedure {0} gerou um erro não tratado.", cmd.CommandText.ToString());

            foreach (SqlParameter parametro in cmd.Parameters)
            {
                if (parametro.Direction == ParameterDirection.Output || parametro.Direction == ParameterDirection.InputOutput)
                {
                    string nmParametro = parametro.ParameterName;

                    if (nmParametro.StartsWith("@"))
                        nmParametro = nmParametro.Substring(1);


                    Util.Helper.ObtemParametrosSistema(parametros, nmParametro, (parametro.Value == DBNull.Value ? null : parametro.Value));

                    if (nmParametro.ToLower().Equals("cd_retorno") && (parametro.Value != null) && parametro.Value != System.DBNull.Value)
                        int.TryParse(parametro.Value.ToString(), out cd_retorno);


                    if (nmParametro.ToLower().Equals("nm_retorno") && (parametro.Value != null) && parametro.Value != System.DBNull.Value)
                        nm_retorno = parametro.Value.ToString();
                }
            }

            if (cdRetornoThrowErro && !cd_retorno.ToString().Equals("0"))
                throw new Exception(nm_retorno);
        }
    }
}
