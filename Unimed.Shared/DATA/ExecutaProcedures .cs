﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Linq;

namespace Unimed.Shared.DATA
{
    public class ExecutaProcedures
    {
        #region "properts"
        public Boolean cdRetornoThrowErro { get; set; }
        public Boolean ThrowErro { get; set; }

        public Int32 CdRetorno { get; set; }
        public String NmRetorno { get; set; }
        public Boolean? DvEdnExecuta { get; set; }
        public Boolean? DvEdnConcluido { get; set; }

        #endregion

        #region "Comando"
        private SqlCommand Comando(SqlConnection conn, String nm_procedure, NameValueCollection objRequest)
        {
            return Comando(conn, nm_procedure, objRequest, null);
        }
        private SqlCommand Comando(SqlConnection conn, String nm_procedure, NameValueCollection objRequest, SqlTransaction tran)
        {
            /* Verifica se foi informada a Procedure  */
            if (String.IsNullOrEmpty(nm_procedure.Trim()))
                throw new Exception("Procedure não informada");

            SqlCommand cmd = conn.CreateCommand();

            if (tran != null)
                cmd.Transaction = tran;

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;
            cmd.CommandText = nm_procedure;


            /* Obtrem os parametros da Procedure */
            List<EN.p_deriva_parametros_procedure> colecaoParametros = DAL.p_deriva_parametros_procedure.DeriveParameters(cmd);

            /* Monta a coleção de paramentros*/
            p_colecao_parametros.MontaColecao(colecaoParametros, objRequest);

            /* Efetua a passagem dos parametros */
            p_colecao_parametros.Parametros(ref cmd, colecaoParametros);


            return cmd;
        }
        private SqlCommand Comando(SqlConnection conn, String nm_procedure, Util.Parametros parametros)
        {
            return Comando(conn, nm_procedure, parametros, null);
        }
        private SqlCommand Comando(SqlConnection conn, String nm_procedure, Util.Parametros parametros, SqlTransaction tran)
        {
            /* Verifica se foi informada a Procedure  */
            if (String.IsNullOrEmpty(nm_procedure.Trim()))
                throw new Exception("Procedure não informada");

            SqlCommand cmd = conn.CreateCommand();

            if (tran != null)
                cmd.Transaction = tran;

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;
            cmd.CommandText = nm_procedure;


            /* Obtrem os parametros da Procedure */
            List<EN.p_deriva_parametros_procedure> colecaoParametros = DAL.p_deriva_parametros_procedure.DeriveParameters(cmd);

            /* Monta a coleção de paramentros*/
            p_colecao_parametros.MontaColecao(colecaoParametros, parametros.GetNameValueCollection());

            /* Efetua a passagem dos parametros */
            p_colecao_parametros.Parametros(ref cmd, colecaoParametros);


            return cmd;
        }
        #endregion

        #region "ExecuteNonQuery"

        public int ExecuteNonQuery(SqlCommand cmd, NameValueCollection objRequest)
        {
            int ret = 0;
            Int32 _cd_retorno = 0;
            String _nm_retorno = String.Empty;
            try
            {
                ret = cmd.ExecuteNonQuery();
                p_colecao_parametros.ParametrosOutPut(cmd, ref objRequest, cdRetornoThrowErro, ref _cd_retorno, ref _nm_retorno);

                CdRetorno = _cd_retorno;
                NmRetorno = _nm_retorno;

            }
            catch (Exception ex)
            {
                if (ThrowErro)
                    throw ex;
            }

            return ret;
        }
        public int ExecuteNonQuery(SqlConnection conn, String nm_procedure, NameValueCollection objRequest, Boolean dv_utiliza_transacao)
        {
            SqlTransaction tran = null;

            if (dv_utiliza_transacao)
                tran = conn.BeginTransaction();

            /* Cria comando */
            SqlCommand cmd = Comando(conn, nm_procedure, objRequest, tran);

            int ret = 0;
            Int32 _cd_retorno = 0;
            String _nm_retorno = String.Empty;

            try
            {
                ret = cmd.ExecuteNonQuery();
                p_colecao_parametros.ParametrosOutPut(cmd, ref objRequest, cdRetornoThrowErro, ref _cd_retorno, ref _nm_retorno);
                CdRetorno = _cd_retorno;
                NmRetorno = _nm_retorno;

                if (dv_utiliza_transacao)
                    tran.Commit();
            }
            catch (Exception ex)
            {
                if (dv_utiliza_transacao)
                    tran.Rollback();
                if (ThrowErro)
                    throw ex;
            }
            finally
            {
                if (dv_utiliza_transacao)
                {
                    tran.Dispose();
                    tran = null;
                }
            }

            return ret;

        }
        public void ExecuteNonQueryInterface(SqlConnection conn, String nm_procedure, NameValueCollection objRequest, Boolean dv_utiliza_transacao)
        {
            SqlTransaction tran = null;

            if (dv_utiliza_transacao)
                tran = conn.BeginTransaction();

            /* Cria comando */
            SqlCommand cmd = Comando(conn, nm_procedure, objRequest, tran);


            Int32 _cd_retorno = 0;
            String _nm_retorno = String.Empty;
            Boolean? _dv_edn_executa = null;
            Boolean? _dv_edn_concluido = null;

            try
            {

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        Util.Helper.ObtemParametrosSistema(objRequest, dr, true);
                    }
                }

                p_colecao_parametros.ParametrosOutPut(cmd, ref objRequest, cdRetornoThrowErro, ref _cd_retorno, ref _nm_retorno);

                if (objRequest.AllKeys.Contains("dv_edn_executa"))
                {
                    if (objRequest["dv_edn_executa"] != null)
                    {
                        if (objRequest["dv_edn_executa"].Equals("0") || objRequest["dv_edn_executa"].ToUpper().Equals("TRUE"))
                            _dv_edn_executa = false;
                    }
                }

                if (objRequest.AllKeys.Contains("dv_edn_concluido"))
                {
                    if (objRequest["dv_edn_concluido"] != null)
                    {
                        if (objRequest["dv_edn_concluido"].Equals("1") || objRequest["dv_edn_concluido"].ToUpper().Equals("TRUE"))
                            _dv_edn_concluido = true;
                    }
                }

                CdRetorno = _cd_retorno;
                NmRetorno = _nm_retorno;
                DvEdnExecuta = _dv_edn_executa;
                DvEdnConcluido = _dv_edn_concluido;

                if (dv_utiliza_transacao)
                    tran.Commit();
                //}
            }
            catch (Exception ex)
            {
                if (dv_utiliza_transacao)
                    tran.Rollback();
                if (ThrowErro)
                    throw ex;
            }
            finally
            {
                if (dv_utiliza_transacao)
                {
                    tran.Dispose();
                    tran = null;
                }
            }

        }
        public void ExecuteNonQueryInterface(SqlConnection conn, String nm_procedure, NameValueCollection objRequest)
        {
            ExecuteNonQueryInterface(conn, nm_procedure, objRequest, false);
        }
        public int ExecuteNonQuery(SqlConnection conn, String nm_procedure, Util.Parametros parametros)
        {
            return ExecuteNonQuery(conn, nm_procedure, parametros, false);
        }
        public int ExecuteNonQuery(SqlCommand cmd, Util.Parametros parametros)
        {
            int ret = 0;
            Int32 _cd_retorno = 0;
            String _nm_retorno = String.Empty;
            try
            {
                ret = cmd.ExecuteNonQuery();
                p_colecao_parametros.ParametrosOutPut(cmd, ref parametros, cdRetornoThrowErro, ref _cd_retorno, ref _nm_retorno);

                CdRetorno = _cd_retorno;
                NmRetorno = _nm_retorno;

            }
            catch (Exception ex)
            {
                if (ThrowErro)
                    throw ex;
            }

            return ret;
        }
        public int ExecuteNonQuery(SqlConnection conn, String nm_procedure, Util.Parametros parametros, Boolean dv_utiliza_transacao)
        {
            SqlTransaction tran = null;

            if (dv_utiliza_transacao)
                tran = conn.BeginTransaction();

            /* Cria comando */
            SqlCommand cmd = Comando(conn, nm_procedure, parametros, tran);

            int ret = 0;
            Int32 _cd_retorno = 0;
            String _nm_retorno = String.Empty;

            try
            {
                ret = cmd.ExecuteNonQuery();
                p_colecao_parametros.ParametrosOutPut(cmd, ref parametros, cdRetornoThrowErro, ref _cd_retorno, ref _nm_retorno);
                CdRetorno = _cd_retorno;
                NmRetorno = _nm_retorno;

                if (dv_utiliza_transacao)
                    tran.Commit();
            }
            catch (Exception ex)
            {
                if (dv_utiliza_transacao)
                    tran.Rollback();
                if (ThrowErro)
                    throw ex;
            }
            finally
            {
                if (dv_utiliza_transacao)
                {
                    tran.Dispose();
                    tran = null;
                }
            }

            return ret;

        }

        #endregion

        #region "ExecuteReader"

        public SqlDataReader ExecuteReader(SqlTransaction tran, SqlConnection conn, String nm_procedure, NameValueCollection objRequest)
        {
            return ExecuteReader(conn, nm_procedure, objRequest, tran);
        }
        public SqlDataReader ExecuteReader(SqlConnection conn, String nm_procedure, NameValueCollection objRequest)
        {
            return ExecuteReader(conn, nm_procedure, objRequest, null);
        }
        public SqlDataReader ExecuteReader(SqlConnection conn, String nm_procedure, NameValueCollection objRequest, SqlTransaction tran)
        {
            SqlCommand cmd;
            Int32 _cd_retorno = 0;
            String _nm_retorno = String.Empty;
            NameValueCollection objRequest_valida = new NameValueCollection();
            
            /* Cria comando */
            if (tran != null)
                cmd = Comando(conn, nm_procedure, objRequest, tran);
            else
                cmd = Comando(conn, nm_procedure, objRequest);

            try
            {
                p_colecao_parametros.ParametrosOutPut(cmd, ref objRequest, cdRetornoThrowErro, ref _cd_retorno, ref _nm_retorno);

                CdRetorno = _cd_retorno;
                NmRetorno = _nm_retorno;
            }
            catch (Exception ex)
            {
                if (ThrowErro)
                    throw ex;
            }

            return cmd.ExecuteReader();

        }
        public SqlDataReader ExecuteReader(SqlTransaction tran, SqlConnection conn, String nm_procedure, Util.Parametros parametros)
        {
            return ExecuteReader(conn, nm_procedure, parametros, tran);
        }
        public SqlDataReader ExecuteReader(SqlConnection conn, String nm_procedure, Util.Parametros parametros)
        {
            return ExecuteReader(conn, nm_procedure, parametros, null);
        }
        public SqlDataReader ExecuteReader(SqlConnection conn, String nm_procedure, Util.Parametros parametros, SqlTransaction tran)
        {
            /* Cria comando */
            SqlCommand cmd;
            Int32 _cd_retorno = 0;
            String _nm_retorno = String.Empty;
            NameValueCollection objRequest_valida = new NameValueCollection();

            /* Cria comando */
            if (tran != null)
                cmd = Comando(conn, nm_procedure, parametros, tran);
            else
                cmd = Comando(conn, nm_procedure, parametros);

            try
            {
                p_colecao_parametros.ParametrosOutPut(cmd, ref parametros, cdRetornoThrowErro, ref _cd_retorno, ref _nm_retorno);

                CdRetorno = _cd_retorno;
                NmRetorno = _nm_retorno;
            }
            catch (Exception ex)
            {
                if (ThrowErro)
                    throw ex;
            }

            return cmd.ExecuteReader();

        }


        #endregion

        #region "ExecuteScalar"

        public Object ExecuteScalar(SqlConnection conn, String nm_procedure, NameValueCollection objRequest, SqlTransaction trn = null)
        {
            /* Cria comando */
            SqlCommand cmd = null;

            if (trn != null)
                cmd = Comando(conn, nm_procedure, objRequest, trn);
            else
                cmd = Comando(conn, nm_procedure, objRequest);

            Object scalar = null;
            Int32 _cd_retorno = 0;
            String _nm_retorno = String.Empty;

            try
            {
                scalar = cmd.ExecuteScalar();
                p_colecao_parametros.ParametrosOutPut(cmd, ref objRequest, cdRetornoThrowErro, ref _cd_retorno, ref _nm_retorno);

                CdRetorno = _cd_retorno;
                NmRetorno = _nm_retorno;

            }
            catch (Exception ex)
            {
                if (ThrowErro)
                    throw ex;
            }

            return scalar;

        }

        public Object ExecuteScalar(SqlConnection conn, String nm_procedure, Util.Parametros parametros, SqlTransaction trn = null)
        {
            /* Cria comando */
            SqlCommand cmd = null;

            if (trn != null)
                cmd = Comando(conn, nm_procedure, parametros, trn);
            else
                cmd = Comando(conn, nm_procedure, parametros);

            Object scalar = null;
            Int32 _cd_retorno = 0;
            String _nm_retorno = String.Empty;

            try
            {
                scalar = cmd.ExecuteScalar();
                p_colecao_parametros.ParametrosOutPut(cmd, ref parametros, cdRetornoThrowErro, ref _cd_retorno, ref _nm_retorno);

                CdRetorno = _cd_retorno;
                NmRetorno = _nm_retorno;
            }
            catch (Exception ex)
            {
                if (ThrowErro)
                    throw ex;
            }

            return scalar;

        }

        #endregion

        #region "ExecuteXmlReader"

        public System.Xml.XmlReader ExecuteXmlReader(SqlConnection conn, String nm_procedure, NameValueCollection objRequest)
        {
            /* Cria comando */
            SqlCommand cmd = Comando(conn, nm_procedure, objRequest);

            System.Xml.XmlReader xdr = null;
            Int32 _cd_retorno = 0;
            String _nm_retorno = String.Empty;

            try
            {
                xdr = cmd.ExecuteXmlReader();
                p_colecao_parametros.ParametrosOutPut(cmd, ref objRequest, cdRetornoThrowErro, ref _cd_retorno, ref _nm_retorno);

                CdRetorno = _cd_retorno;
                NmRetorno = _nm_retorno;
            }
            catch (Exception ex)
            {
                if (ThrowErro)
                    throw ex;
            }

            return xdr;

        }

        public System.Xml.XmlReader ExecuteXmlReader(SqlConnection conn, String nm_procedure, Util.Parametros parametros)
        {
            /* Cria comando */
            SqlCommand cmd = Comando(conn, nm_procedure, parametros);

            System.Xml.XmlReader xdr = null;
            Int32 _cd_retorno = 0;
            String _nm_retorno = String.Empty;

            try
            {
                xdr = cmd.ExecuteXmlReader();
                p_colecao_parametros.ParametrosOutPut(cmd, ref parametros, cdRetornoThrowErro, ref _cd_retorno, ref _nm_retorno);

                CdRetorno = _cd_retorno;
                NmRetorno = _nm_retorno;
            }
            catch (Exception ex)
            {
                if (ThrowErro)
                    throw ex;
            }

            return xdr;

        }
        #endregion

        #region "ExecuteDataSet"
        public DataSet ExecuteDataSet(SqlConnection conn, String nm_procedure, NameValueCollection objRequest)
        {
            /* Cria comando */
            SqlCommand cmd = Comando(conn, nm_procedure, objRequest);

            DataSet dsResult = new DataSet();
            Int32 _cd_retorno = 0;
            String _nm_retorno = String.Empty;

            try
            {
                using (var sda = new SqlDataAdapter(nm_procedure, conn))
                {

                    sda.SelectCommand = cmd;
                    sda.Fill(dsResult);
                }

                CdRetorno = _cd_retorno;
                NmRetorno = _nm_retorno;

            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            catch (Exception ex)
            {
                if (ThrowErro)
                    throw ex;
            }

            return dsResult;

        }
        #endregion

    }
}
